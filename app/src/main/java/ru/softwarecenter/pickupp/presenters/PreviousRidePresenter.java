package ru.softwarecenter.pickupp.presenters;


import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.ActiveRideTravelDetailsAdapter;
import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.TravelDetailsAdapter;
import ru.softwarecenter.pickupp.interfaces.PreviousRideInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;

public class PreviousRidePresenter extends BasePresenter<PreviousRideInterface.View> implements PreviousRideInterface.Presenter{
    private List<String> aboutTravelerListTitle;
    private HashMap<String, List<String>> aboutTravelerListDetail;
    private List<String> aboutTravelerDetailsList;
    private List<String> travelDetailsListTitle;
    private HashMap<String, List<String>> travelDetailsListDetail;
    private List<String> travelDetailsDetailsList;

    private ActiveRideTravelDetailsAdapter adapterAboutTraveler;
    private TravelDetailsAdapter adapterTravelDetails;

    /*@Override
    public void initAboutTravelerArrays() {
        aboutTravelerListTitle = new ArrayList<>();
        aboutTravelerDetailsList = new ArrayList<>();
        aboutTravelerListDetail = new HashMap<>();
        aboutTravelerListTitle.add("О попутчике");
        aboutTravelerDetailsList.add("");
    }*/

    @Override
    public void initTravelDetailsArrays() {
        travelDetailsListTitle = new ArrayList<>();
        travelDetailsDetailsList = new ArrayList<>();
        travelDetailsListDetail = new HashMap<>();
        travelDetailsListTitle.add("Детали поездки");
        travelDetailsDetailsList.add("");
    }

    /*@Override
    public void getAdapterAboutTraveler(Activity activity) {
        aboutTravelerListDetail.clear();
        aboutTravelerListDetail.put(aboutTravelerListTitle.get(0), aboutTravelerDetailsList);
        adapterAboutTraveler = new ActiveRideTravelDetailsAdapter(activity, aboutTravelerListTitle, aboutTravelerListDetail);
        getView().setAboutTravelerListView(adapterAboutTraveler);
    }*/

    @Override
    public void getAdapterTravelDetails(Activity activity) {
        travelDetailsListDetail.clear();
        travelDetailsListDetail.put(travelDetailsListTitle.get(0), travelDetailsDetailsList);
        adapterTravelDetails = new TravelDetailsAdapter(activity, travelDetailsListTitle, travelDetailsListDetail);
        getView().setTravelDetailsListView(adapterTravelDetails);
    }
}
