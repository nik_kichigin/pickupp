package ru.softwarecenter.pickupp.presenters;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.EnterInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;



public class EnterPresenter extends BasePresenter<EnterInterface.View> implements EnterInterface.Presenter {

    @Override
    public void phoneFiled(String phone) {
        if(phone.length() == 10){
            getView().updateButtonSendPhone();
            getView().setFocusOnCodeField();
        }else{
            getView().showMessage(R.string.phoneNumberLenghError);
        }
    }

    @Override
    public void codeFiled(String code, String phone) {
        if(phone.length() == 10){
            if(code.length() == 4){
                getView().openEnterAdditionalInfoScreen();
            }else{
                getView().showMessage(R.string.codeLengthError);
            }
        }else{
            getView().showMessage(R.string.phoneNumberLenghError);
        }

    }
}
