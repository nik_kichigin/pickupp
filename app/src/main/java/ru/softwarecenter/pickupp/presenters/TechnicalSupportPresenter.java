package ru.softwarecenter.pickupp.presenters;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.CategorySupportAdapter;
import ru.softwarecenter.pickupp.interfaces.PreviousRideInterface;
import ru.softwarecenter.pickupp.interfaces.TechnicalSupportInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.SupportMessage;

public class TechnicalSupportPresenter extends BasePresenter<TechnicalSupportInterface.View> implements TechnicalSupportInterface.Presenter{

    private List<String> categoryListTitle;
    private HashMap<String, List<String>> categoryListDetail;
    private List<String> categoryDetailsList;

    private List<String> pathComunicateListTitle;
    private HashMap<String, List<String>> pathComunicateListDetail;
    private List<String> pathComunicateDetailsList;

    private CategorySupportAdapter categoryListAdapter;
    private CategorySupportAdapter pathComunicateListAdapter;

    private boolean isCategoryAdded = false;
    private boolean isPathComunicateAdded = false;
    private boolean isEmailFieldOpen = false;

    private SupportMessage message = new SupportMessage();

    @Override
    public void initCategoryArrays() {
        categoryListTitle = new ArrayList<>();
        categoryDetailsList = new ArrayList<>();
        categoryListDetail = new HashMap<>();
        categoryListTitle.add("Выберите категорию");
        categoryDetailsList.add("Категория 1");
        categoryDetailsList.add("Категория 2");
    }

    @Override
    public void initPathComunicateArrays() {
        pathComunicateListTitle = new ArrayList<>();
        pathComunicateDetailsList = new ArrayList<>();
        pathComunicateListDetail = new HashMap<>();
        pathComunicateListTitle.add("Как с вами связаться?");
        pathComunicateDetailsList.add("Телефон");
        pathComunicateDetailsList.add("Электронная почта");
    }

    @Override
    public void getAdapterCathegory(Activity activity) {
        categoryListDetail.clear();
        categoryListDetail.put(categoryListTitle.get(0), categoryDetailsList);
        categoryListAdapter = new CategorySupportAdapter(activity, categoryListTitle, categoryListDetail);
        getView().setCathegoryListView(categoryListAdapter);
    }

    @Override
    public void getAdapterPathComunicate(Activity activity) {
        pathComunicateListDetail.clear();
        pathComunicateListDetail.put(pathComunicateListTitle.get(0), pathComunicateDetailsList);
        pathComunicateListAdapter = new CategorySupportAdapter(activity, pathComunicateListTitle, pathComunicateListDetail);
        getView().setPathComunicateListView(pathComunicateListAdapter);
    }

    @Override
    public void onCategoryChildItemClick(int groupPosition, int childPosition) {
        if (!isCategoryAdded) {
            isCategoryAdded = true;
        }

        String text = categoryListDetail.get(
                categoryListTitle.get(groupPosition)).get(
                childPosition);

        setCategory(text);

        categoryListTitle.clear();
        categoryListTitle.add(groupPosition, text);
        categoryListDetail.clear();
        categoryListDetail.put(categoryListTitle.get(0), categoryDetailsList);
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPathComunicateChildItemClick(int groupPosition, int childPosition) {
        if (!isPathComunicateAdded) {
            isPathComunicateAdded = true;
        }

        String text = pathComunicateListDetail.get(
                pathComunicateListTitle.get(groupPosition)).get(
                childPosition);

        setPathComunicate(text);

        pathComunicateListTitle.clear();
        pathComunicateListTitle.add(groupPosition, text);
        pathComunicateListDetail.clear();
        pathComunicateListDetail.put(pathComunicateListTitle.get(0), pathComunicateDetailsList);
        pathComunicateListAdapter.notifyDataSetChanged();

    }

    @Override
    public void setEmailFieldOpenState(boolean isEmailFieldOpen) {
        this.isEmailFieldOpen = isEmailFieldOpen;
    }

    @Override
    public void onSendMessageBtnClick() {
        if (!isCategoryAdded) {
            getView().showMessage(R.string.selectCategoryError);
        } else if (!isPathComunicateAdded) {
            getView().showMessage(R.string.selectPathCommunicateError);
        } else if (isEmailFieldOpen) {
            String emailText = getView().getEmailText();
            if (emailText.length() == 0) {
                getView().showMessage(R.string.addEmailError);
            } else if (getView().getMessageText().length() == 0) {
                getView().showMessage(R.string.addMessageTextError);
            } else {
                getMessageInfo();
            }
        } else if (getView().getMessageText().length() == 0) {
            getView().showMessage(R.string.addMessageTextError);
        } else {
            getMessageInfo();
        }
    }

    private void getMessageInfo() {
        message.setMessage(getView().getMessageText());
        if (isEmailFieldOpen) {
            message.setEmail(getView().getEmailText());
        }
        getView().sendMessage();
    }

    private void setCategory(String category) {
        message.setCategory(category);
    }

    private void setPathComunicate(String path_comunicate) {
        message.setCommunication_path(path_comunicate);
    }
}
