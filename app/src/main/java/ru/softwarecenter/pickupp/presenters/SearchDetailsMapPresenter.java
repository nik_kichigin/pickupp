package ru.softwarecenter.pickupp.presenters;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.data_store.database.AddressesFromMapHandler;
import ru.softwarecenter.pickupp.data_store.database.AddressesToMapHandler;
import ru.softwarecenter.pickupp.interfaces.SearchDetailsMapInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.SearchDetailsMapActivity;
import ru.softwarecenter.pickupp.views.SearchRideActivity;

import static ru.softwarecenter.pickupp.utils.Constants.MAP_STATE_SEARCH_TO;

public class SearchDetailsMapPresenter extends BasePresenter<SearchDetailsMapInterface.View> implements SearchDetailsMapInterface.Presenter, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener{
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 54679;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private boolean isUiHide = false;
    private Ride ride = new Ride();
    private int currentMapState;

    @Override
    public void setRide(Ride ride) {
        this.ride = ride;
    }

    @Override
    public void setCurrentMapState(String mapState) {
        if (mapState.equals("from")) {
            currentMapState = Constants.MAP_STATE_SEARCH_FROM;
            getView().setViewStateFrom();
        } else {
            currentMapState = Constants.MAP_STATE_SEARCH_TO;
            getView().setViewStateTo();
        }
    }

    @Override
    public Ride getRide() {
        return ride;
    }

    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getView().getMapFragment().getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void openSheet() {
        if(currentMapState == Constants.MAP_STATE_SEARCH_FROM){
            getView().openFromBottomSheet();
        }else{
            getView().openToBottomSheet();
        }
    }

    @Override
    public void bottomBackBtnClick() {
        LatLng latLng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
        String address = getCompleteAddressString(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);

        if(currentMapState == Constants.MAP_STATE_SEARCH_FROM){
            if (address != null && !address.equals("")) {
                Street streetFrom = new Street();
                streetFrom.setAdress(address);
                streetFrom.setPoint(latLng);
                ride.setFrom(streetFrom);
                getView().backToSearchScreen(ride);
            } else {
                getView().showMessage(R.string.addressFromNotSelected);
            }
        }else if(currentMapState == MAP_STATE_SEARCH_TO){
            if (address != null && !address.equals("")) {
                Street streetTo = new Street();
                streetTo.setAdress(address);
                streetTo.setPoint(latLng);
                ride.setTo(streetTo);
                getView().backToSearchScreen(ride);
            } else {
                getView().showMessage(R.string.addressToNotSelected);
            }
        }
    }

    @Override
    public void onAddressFromCardClick(Address address) {
        AddressesFromMapHandler db = new AddressesFromMapHandler((Context) getView());

        String country = address.getCountryName();
        String city = address.getLocality();
        String street = address.getThoroughfare();
        String house = address.getSubThoroughfare();

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        moveCamera(lat, lng);

        db.addAddress(house, street, city, country, lat, lng);
    }

    @Override
    public void onAddressToCardClick(Address address) {
        AddressesToMapHandler db = new AddressesToMapHandler((Context) getView());

        String country = address.getCountryName();
        String city = address.getLocality();
        String street = address.getThoroughfare();
        String house = address.getSubThoroughfare();

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        moveCamera(lat, lng);

        db.addAddress(house, street, city, country, lat, lng);
    }

    @Override
    public List<Address> getAdresses(String location, Context context) {

        List<Address> addressList = new ArrayList<>();

        LatLngBounds bounds = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
        //AutocompleteFilter autocompleteFilter = new AutocompleteFilter();

        if (mGoogleApiClient != null) {
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, location,
                                    bounds, null);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                //Toast.makeText((Context) getView(), "Error: " + status.toString(), Toast.LENGTH_SHORT).show();

                //Log.e(TAG, "Error getting place predictions: " + status.toString());
                autocompletePredictions.release();

            }

            // Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount() + " predictions.");
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));

                Address address = new Address(null);

                Places.GeoDataApi.getPlaceById(mGoogleApiClient,prediction.getPlaceId()).setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);

                            address.setLongitude(myPlace.getLatLng().longitude);
                            address.setLatitude(myPlace.getLatLng().latitude);

                            //Log.i(TAG, "Place found: " + myPlace.getName());
                        } else {
                            //Log.e(TAG, "Place not found");
                        }
                        places.release();
                    }
                });

                address.setThoroughfare(prediction.getPrimaryText(null).toString());
                address.setCountryName(prediction.getSecondaryText(null).toString());
                addressList.add(address);
            }
            // Buffer release
            autocompletePredictions.release();
        }
        return addressList;
    }


    class PlaceAutocomplete {

        CharSequence placeId;
        CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }

    private void moveCamera(double lat, double lng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14 ));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);

        if (currentMapState == Constants.MAP_STATE_SEARCH_FROM) {
            moveCamera(ride.getFrom().getPoint().latitude, ride.getFrom().getPoint().longitude);
        } else {
            moveCamera(ride.getTo().getPoint().latitude, ride.getTo().getPoint().longitude);
        }
    }



    @Override
    public void onCameraMove() {
        if(!isUiHide) {
            getView().hideUI();
            isUiHide = true;
        }
    }

    @Override
    public void onCameraIdle() {
        getView().showUI();
        isUiHide = false;
        getView().updateTitle(getCompleteAddressString(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude));
    }



    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";

        Geocoder geocoder = new Geocoder(((SearchDetailsMapActivity) getView()), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);

            if (addresses != null) {

                Address returnedAddress = addresses.get(0);
                if(returnedAddress.getThoroughfare() != null && returnedAddress.getSubThoroughfare() != null) {
                    strAdd = returnedAddress.getThoroughfare() + ", " + returnedAddress.getSubThoroughfare();
                }
            } else {
                Log.w("My Current", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current", "Canont get Address!");
        }
        return strAdd;
    }

}
