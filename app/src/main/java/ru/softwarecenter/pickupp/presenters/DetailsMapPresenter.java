package ru.softwarecenter.pickupp.presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.DetailsMapInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.DetailsMapActivity;

public class DetailsMapPresenter extends BasePresenter<DetailsMapInterface.View> implements DetailsMapInterface.Presenter, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener {
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 54679;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private boolean isUiHide = false;
    private Ride ride = new Ride();
    private int currentMapState = Constants.MAP_STATE_SEARCH_FROM;


    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getView().getMapFragment().getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void locationClick() {
        if(mMap.isMyLocationEnabled()){
            Location mLastKnownLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if(mLastKnownLocation != null){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),
                        mLastKnownLocation.getLongitude()), 14 ));
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        if(checkLocationPermission()){
            updateLocation();
        }

        final LatLng FROM = new LatLng(59.960453, 30.3449);
        mMap.addMarker(new MarkerOptions()
                .position(FROM)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.buble_from)));

        final LatLng TO = new LatLng(59.9763302, 30.4010657);
        mMap.addMarker(new MarkerOptions()
                .position(TO)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.buble_to)));

        String markerState = getView().getMarkerState();
        if (markerState.equals("from")) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom( FROM, 14 ));
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom( TO, 14 ));
        }
    }

    @SuppressWarnings("MissingPermission")
    public void updateLocation(){
        if(mMap == null){
            return;
        }
        mMap.setMyLocationEnabled(true);
        Location mLastKnownLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if(mLastKnownLocation != null){
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),
                    mLastKnownLocation.getLongitude()), 14 ));
        }
    }

    @Override
    public void onCameraMove() {
        if(!isUiHide) {
            getView().hideUI();
            isUiHide = true;
        }
    }

    @Override
    public void onCameraIdle() {
        getView().showUI();
        isUiHide = false;
        getView().updateTitle(getCompleteAddressString(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude));
    }

    private boolean checkLocationPermission(){
        AppCompatActivity activity = ((DetailsMapActivity) getView());
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                getView().showPermissionMessage();
            }else{
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_REQUEST_CODE);
            }
            return false;
        }
        return true;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";

        Geocoder geocoder = new Geocoder(((DetailsMapActivity) getView()), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);

            if (addresses != null) {

                Address returnedAddress = addresses.get(0);
                if(returnedAddress.getThoroughfare() != null && returnedAddress.getSubThoroughfare() != null) {
                    strAdd = returnedAddress.getThoroughfare() + ", " + returnedAddress.getSubThoroughfare();
                }
            } else {
                Log.w("My Current", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current", "Canont get Address!");
        }
        return strAdd;
    }
}
