package ru.softwarecenter.pickupp.presenters;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.data_store.database.AddressesFromMapHandler;
import ru.softwarecenter.pickupp.data_store.database.AddressesToMapHandler;
import ru.softwarecenter.pickupp.interfaces.MapInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.MapActivity;

import static android.app.Activity.RESULT_OK;
import static ru.softwarecenter.pickupp.utils.Constants.MAP_STATE_SEARCH_TO;

public class MapPresenter extends BasePresenter<MapInterface.View> implements MapInterface.Presenter, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener {
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 54679;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private boolean isUiHide = false;
    private Ride ride = new Ride();
    private int currentMapState = Constants.MAP_STATE_SEARCH_FROM;

    private LatLngBounds bounds = new LatLngBounds(new LatLng(43.1056200, 131.8735300), new LatLng(54.707390, 20.507307));


    @Override
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(isViewAttached()) {
            getView().getMapFragment().getMapAsync(this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void driverClick() {
        if(ride.isDriverOn()){
            getView().driverBtnDisabled();
        }else{
            if(currentMapState == Constants.MAP_STATE_SEARCH_FROM){
                getView().driverBtnEnabledFrom();
            }else{
                getView().driverBtnEnabledTo();
            }
        }
        ride.setDriverOn(!ride.isDriverOn());
    }

    @Override
    public void openSheet() {
        if(currentMapState == Constants.MAP_STATE_SEARCH_FROM){
            getView().openFromBottomSheet();
        }else{
            getView().openToBottomSheet();
        }
    }

    @Override
    public void searhBtnClick() {
        LatLng latLng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
        String address = getCompleteAddressString(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);

        if(currentMapState == Constants.MAP_STATE_SEARCH_FROM){
            if (address != null && !address.equals("")) {
                changeStateSearch();
                getView().openToBottomSheet();
                Street streetFrom = new Street();
                streetFrom.setAdress(address);
                streetFrom.setPoint(latLng);
                ride.setFrom(streetFrom);
            } else {
                getView().showMessage(R.string.addressFromNotSelected);
            }
        }else if(currentMapState == MAP_STATE_SEARCH_TO){
            if (address != null && !address.equals("")) {
                Street streetTo = new Street();
                streetTo.setAdress(address);
                streetTo.setPoint(latLng);
                ride.setTo(streetTo);
                getView().openSearchScreen(ride);
            } else {
                getView().showMessage(R.string.addressToNotSelected);
            }
        }
    }

    @Override
    public void onFromViewClick() {
        changeStateSearch();
    }

    @Override
    public boolean isRideDriverOn() {
        return ride.isDriverOn();
    }

    @Override
    public void onAddressFromCardClick(Address address) {
        AddressesFromMapHandler db = new AddressesFromMapHandler((Context) getView());

        String country = address.getCountryName();
        String city = address.getLocality();
        String street = address.getThoroughfare();
        String house = address.getSubThoroughfare();
        double lat = address.getLatitude();
        double lng = address.getLongitude();
        moveCamera(lat, lng);
        db.addAddress(house, street, city, country, lat, lng);
    }

    @Override
    public void onAddressToCardClick(Address address) {
        AddressesToMapHandler db = new AddressesToMapHandler((Context) getView());

        String country = address.getCountryName();
        String city = address.getLocality();
        String street = address.getThoroughfare();
        String house = address.getSubThoroughfare();


        double lat = address.getLatitude();
        double lng = address.getLongitude();
        moveCamera(lat, lng);

        db.addAddress(house, street, city, country, lat, lng);
    }

    @Override
    public void setMapGestures(boolean isMenuOpened) {
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomGesturesEnabled(!isMenuOpened);
        uiSettings.setScrollGesturesEnabled(!isMenuOpened);
        uiSettings.setTiltGesturesEnabled(!isMenuOpened);
        uiSettings.setRotateGesturesEnabled(!isMenuOpened);
    }

    @Override
    public void onBackPressed() {
        if (currentMapState == Constants.MAP_STATE_SEARCH_TO) {
            changeStateSearch();
        } else {
            getView().closeApp();
        }
    }

    @Override
    public void backToMapEvent(int resultCode, Ride ride) {
        //this.ride = ride;
        currentMapState = Constants.MAP_STATE_SEARCH_TO;//TODO Названия перепутаны кажись
        changeStateSearch();
    }

    @Override
    @SuppressLint("SimpleDateFormat")
    public void setTimeRide(String time) {
        long longDate = 1;

        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
        try {
            Date d = f.parse(time);
            longDate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        ride.setTime(longDate);
    }

    @Override
    public String getFromText() {
        return ride.getFrom().getAdress();
    }

    @Override
    public List<Address> getAdresses(String location, Context context) {

        List<Address> addressList = new ArrayList<>();

        //LatLngBounds bounds = new LatLngBounds(new LatLng(59.89444, 30.26417), new LatLng(59.89444, 30.26417));
        //AutocompleteFilter autocompleteFilter = new AutocompleteFilter();

        if (mGoogleApiClient != null) {
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, location,
                                    bounds, null);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                //Toast.makeText((Context) getView(), "Error: " + status.toString(), Toast.LENGTH_SHORT).show();

                //Log.e(TAG, "Error getting place predictions: " + status.toString());
                autocompletePredictions.release();

            }

            // Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount() + " predictions.");
            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));

                Address address = new Address(null);

                Places.GeoDataApi.getPlaceById(mGoogleApiClient,prediction.getPlaceId()).setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);

                            address.setLongitude(myPlace.getLatLng().longitude);
                            address.setLatitude(myPlace.getLatLng().latitude);

                            //Log.i(TAG, "Place found: " + myPlace.getName());
                        } else {
                            //Log.e(TAG, "Place not found");
                        }
                        places.release();
                    }
                });

                address.setThoroughfare(prediction.getPrimaryText(null).toString());
                address.setCountryName(prediction.getSecondaryText(null).toString());
                addressList.add(address);
            }
            // Buffer release
            autocompletePredictions.release();
        }
        return addressList;
    }


    class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }

    private void moveCamera(double lat, double lng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14 ));
    }

    private void changeStateSearch() {
        switch (currentMapState) {
            case Constants.MAP_STATE_SEARCH_FROM:
                currentMapState = MAP_STATE_SEARCH_TO;
                getView().changeViewStateTo();
                break;
            case MAP_STATE_SEARCH_TO:
                currentMapState = Constants.MAP_STATE_SEARCH_FROM;
                getView().changeViewStateFrom();
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void locationClick() {
        if(mMap.isMyLocationEnabled()){
            Location mLastKnownLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if(mLastKnownLocation != null){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),
                        mLastKnownLocation.getLongitude()), 14 ));
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        if(checkLocationPermission()){
            updateLocation();
        }

        mMap.setOnMapClickListener(latLng -> {
            if (getView().isMenuOpen()) {
                getView().closeMenu();
            }
        });
    }

    @SuppressWarnings("MissingPermission")
    public void updateLocation(){
        if(mMap == null){
            return;
        }
        mMap.setMyLocationEnabled(true);
        Location mLastKnownLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if(mLastKnownLocation != null){
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(),
                    mLastKnownLocation.getLongitude()), 14 ));

            updateBoundsCoordinates(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
        }
    }

    private void updateBoundsCoordinates(double latitude, double longitude) {
        bounds = new LatLngBounds(new LatLng(latitude, longitude), new LatLng(latitude, longitude));
    }

    @Override
    public void onCameraMove() {
        if(!isUiHide) {
            getView().hideUI();
            isUiHide = true;
        }
    }

    @Override
    public void onCameraIdle() {
        getView().showUI();
        isUiHide = false;
        getView().updateTitle(getCompleteAddressString(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude));
    }

    private boolean checkLocationPermission(){
        AppCompatActivity activity = ((MapActivity) getView());
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                getView().showPermissionMessage();
            }else{
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_REQUEST_CODE);
            }
            return false;
        }
        return true;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";

        Geocoder geocoder = new Geocoder(((MapActivity) getView()), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);

            if (addresses != null) {

                Address returnedAddress = addresses.get(0);
                if(returnedAddress.getThoroughfare() != null && returnedAddress.getSubThoroughfare() != null) {
                    strAdd = returnedAddress.getThoroughfare() + ", " + returnedAddress.getSubThoroughfare();
                }
            } else {
                Log.w("My Current", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current", "Canont get Address!");
        }
        return strAdd;
    }
}
