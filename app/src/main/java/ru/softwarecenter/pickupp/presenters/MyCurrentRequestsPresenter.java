package ru.softwarecenter.pickupp.presenters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMyCurrentRequestsAdapter;
import ru.softwarecenter.pickupp.interfaces.MyCurrentRequestsInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.utils.Constants;

public class MyCurrentRequestsPresenter extends BasePresenter<MyCurrentRequestsInterface.View> implements MyCurrentRequestsInterface.Presenter {

    @Override
    public RVMyCurrentRequestsAdapter getMyCurrentRequestsAdapter() {
        List<Ride> requestsArray = new ArrayList<>();
        for (int i = 0; i < 4; i ++) {
            Ride ride = new Ride();
            ride.setTime(Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS);
            Street from = new Street();
            Street to = new Street();
            from.setAdress("Науки, 13");
            to.setAdress("Бутлерова, 25");
            ride.setFrom(from);
            ride.setTo(to);

            requestsArray.add(ride);
        }

        return new RVMyCurrentRequestsAdapter(requestsArray);
    }
}
