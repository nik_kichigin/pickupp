package ru.softwarecenter.pickupp.presenters;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.SearchRideInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.fragments.TimeDialog;

public class SearchRidePresenter extends BasePresenter<SearchRideInterface.View> implements SearchRideInterface.Presenter {
    private Ride ride = new Ride();
    private boolean additionalOpen = false;
    AlertDialog dialog = null;

    @Override
    public void setRide(Ride ride) {
        this.ride = ride;
    }

    /*@Override
    public Ride getRide() {
        return ride;
    }*/

    @Override
    public void addressFromCilck() {
        getView().openSearchDetailsMap(ride, "from");
    }

    @Override
    public void addressToCilck() {
        getView().openSearchDetailsMap(ride, "to");
    }

    @Override
    public void changeAdressClick() {
        Street tmp = ride.getFrom();
        ride.setFrom(ride.getTo());
        ride.setTo(tmp);
        getView().changeAdresses(ride.getFrom().getAdress(), ride.getTo().getAdress());
    }

    @Override
    public void radiusAfterClick() {

    }

    @Override
    public void radiusBeforeClick() {

    }

    @Override
    public void additionalClick() {
        if(additionalOpen){
            additionalOpen = false;
            getView().closeAdditionalInfo();
        }else{
            additionalOpen = true;
            getView().openAdditionalInfo();
        }
    }

    @Override
    public void searchChose(int isPickUpp) {
        if(ride.getSearchPickUpp() == isPickUpp){
            getView().setCarAndTaxiDisable();
            ride.setSearchPickUpp(-1);
            return;
        }
        if(ride.getSearchPickUpp() != 0){
            getView().setCarAndTaxiDisable();
            if(isPickUpp == 1){
                getView().setCarActive();
            }else{
                getView().setTaxiActive();
            }
            getView().showSearchRecyclerView();
        }
        ride.setSearchPickUpp(isPickUpp);

    }

    @Override
    public void costAdded(String s) {

    }

    @Override
    public void peopleAdded(String s) {

    }

    @Override
    public void bagClicked() {
        if(ride.isHasBag()){
            ride.setHasBag(false);
            getView().setBagDisable();
        }else{
            ride.setHasBag(true);
            getView().setBagActive();
        }
    }

    @Override
    public void setAddressText() {
        //Toast.makeText((Context) getView(), ride.getFrom().getAdress(), Toast.LENGTH_SHORT).show();
        //Toast.makeText((Context) getView(), ride.getTo().getAdress(), Toast.LENGTH_SHORT).show();

        getView().changeAdresses(ride.getFrom().getAdress(), ride.getTo().getAdress());
    }

    @Override
    public void onLetsRideBtnClick() {
        getView().backToMapScreen(ride);
    }

    @Override
    public void openTimeDepartureDialog() {
        if (dialog == null) {
            createTimeDepartureDialog();
        } else {
            dialog.show();
        }
    }

    private void createTimeDepartureDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder((Context) getView());
        builder.setTitle("Когда вам удобно выехать?");

        View view;
        view = ((LayoutInflater) ((Context) getView()).getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_departure_dialog_layout, null);

        LinearLayout startRideTimeLayout = view.findViewById(R.id.start_ride_time_layout);
        TextView startRideTimeText = view.findViewById(R.id.start_ride_time_text);
        startRideTimeText.setText(new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS));

        LinearLayout earlyTimeLayout = view.findViewById(R.id.early_time_layout);
        TextView earlyTimeText = view.findViewById(R.id.early_time_txt);

        LinearLayout lateTimeLayout = view.findViewById(R.id.late_time_layout);
        TextView lateTimeText = view.findViewById(R.id.late_time_txt);

        TextView cancelBtn = view.findViewById(R.id.cancel_btn);
        TextView okBtn = view.findViewById(R.id.ok_btn);

        startRideTimeLayout.setOnClickListener(view1 -> {
            getView().openTimeDepartDialog(startRideTimeText);
        });

        earlyTimeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getView().openNumberPickerDialog(earlyTimeText);
            }
        });

        lateTimeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getView().openNumberPickerDialog(lateTimeText);
            }
        });

        dialog = builder.create();
        dialog.setView(view);
        dialog.show();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                String startTime = startRideTimeText.getText().toString();
                int earlyTime = Integer.parseInt(earlyTimeText.getText().toString());
                int lateTime = Integer.parseInt(lateTimeText.getText().toString());
                String earlyLateTimeText;
                if (earlyTime == lateTime) {
                    earlyLateTimeText = "± " + String.valueOf(earlyTime) + " минут";
                } else {
                    earlyLateTimeText = "+ " + String.valueOf(lateTime) + " минут; - " + String.valueOf(earlyTime) + " минут";
                }

                getView().updateToolbarTextFields(startTime, earlyLateTimeText);

                ride.setEarlyTime(earlyTime);
                ride.setLateTime(lateTime);

                long longTime = 1;

                SimpleDateFormat f = new SimpleDateFormat("HH:mm");
                try {
                    Date d = f.parse(startTime);
                    longTime = d.getTime();

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ride.setTime(longTime);
            }
        });
    }


    @Override
    public Ride getRide() {
        return ride;
    }

}
