package ru.softwarecenter.pickupp.presenters;

import android.widget.TextView;

import ru.softwarecenter.pickupp.interfaces.MyProfileInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;

public class MyProfilePresenter extends BasePresenter<MyProfileInterface.View> implements MyProfileInterface.Presenter {

    private String statusText;
    private String aboutYourselfText;

    @Override
    public void saveStatusClick() {
        this.statusText = getView().getStatusText();
        getView().setStatusToUI(statusText);
    }

    @Override
    public void saveAboutYourselfClick() {
        this.aboutYourselfText = getView().getAboutYourselfText();
        getView().setAboutYourselfToUI(aboutYourselfText);
    }
}
