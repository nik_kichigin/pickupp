package ru.softwarecenter.pickupp.presenters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;

import java.util.Calendar;
import java.util.List;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVHistoryRequestsAdapter;
import ru.softwarecenter.pickupp.data_store.database.HistoryRequestsHandler;
import ru.softwarecenter.pickupp.interfaces.HistoryRequestsInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.utils.Constants;

public class HistoryRequestsPresenter extends BasePresenter<HistoryRequestsInterface.View> implements HistoryRequestsInterface.Presenter{

    @Override
    public void initRecyclerView(Context context) {
        HistoryRequestsHandler db = new HistoryRequestsHandler(context);
        List<Ride> requestsArray = db.getAllHistoryRequests();
        if (requestsArray.size() == 0) {
            for (int i = 0; i < 2; i ++) {
                long time = Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS;
                String from_adress = "Садовая, " + String.valueOf(i);
                String to_adress = "Газетный, 15";

                db.addRequests(i, time, from_adress, to_adress);
            }
            requestsArray = db.getAllHistoryRequests();
            createAdapter(requestsArray);
        } else {
            createAdapter(requestsArray);
        }
    }

    private void createAdapter(List<Ride> requestsArray) {
        RecyclerView recyclerView = getView().getRecyclerView();
        RVHistoryRequestsAdapter adapter = new RVHistoryRequestsAdapter(requestsArray, recyclerView);

        getView().setAdapterToRV(adapter);

        adapter.setOnLoadMoreListener(() -> {
            //Log.e("haint", "Load More");
            requestsArray.add(null);
            adapter.notifyItemInserted(requestsArray.size() - 1);
            //Load more data for reyclerview
            new Handler().postDelayed(new Runnable() {
                @Override public void run() {
                    //Log.e("haint", "Load More 2");
                    //Remove loading item
                    requestsArray.remove(requestsArray.size() - 1);
                    adapter.notifyItemRemoved(requestsArray.size());
                    //Load data
                    int index = requestsArray.size();
                    int end = index + 20;
                    for (int i = index; i < end; i++) {
                        Ride ride = new Ride();
                        ride.setTime(Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS);
                        Street streetFrom = new Street();
                        streetFrom.setAdress("Садовая, " + String.valueOf(i));
                        ride.setFrom(streetFrom);
                        Street streetTo = new Street();
                        streetTo.setAdress("Газетный, 15");
                        ride.setTo(streetTo);
                        requestsArray.add(ride);
                    }
                    adapter.notifyDataSetChanged();
                    adapter.setLoaded();
                }
            }, 5000);
        });


    }


}
