package ru.softwarecenter.pickupp.presenters;

import ru.softwarecenter.pickupp.interfaces.RatingRideInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;

public class RatingRidePresenter extends BasePresenter<RatingRideInterface.View> implements RatingRideInterface.Presenter{

    private float ratingValue;

    @Override
    public void setRatingValue(float value) {
        ratingValue = value;
    }
}
