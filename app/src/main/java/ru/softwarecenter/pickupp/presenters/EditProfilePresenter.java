package ru.softwarecenter.pickupp.presenters;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.EditProfileInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.User;
import ru.softwarecenter.pickupp.utils.GenericFileProvider;
import ru.softwarecenter.pickupp.utils.ImageUtils;
import ru.softwarecenter.pickupp.views.EditProfileActivity;

public class EditProfilePresenter extends BasePresenter<EditProfileInterface.View> implements EditProfileInterface.Presenter {
    public static final int PERMISSION_STORAGE_REQUEST_CODE = 54678;

    private User user;
    private String photoPath = null;
    private boolean isSexAdded = false;

    public EditProfilePresenter() {
        user = new User();
    }

    public boolean isAvatarAdded(){
        return user.getAvatar() != null;
    }

    @Override
    public void setSex(int sex) {
        if(user.getSex() == -1){
            user.setSex(sex);
            if(sex == 0){
                getView().setWomanSexSelected();
            }else{
                getView().setManSexSelected();
            }
        }else{
            getView().setSexDeselected();
            user.setSex(-1);
            setSex(sex);
        }
        if (!isSexAdded) {
            isSexAdded = true;
        }
    }

    @Override
    public void setName(String name) {
        user.setName(name);
    }

    @Override
    public void clearName() {
        user.setName(null);
    }

    @Override
    public void clearAge() {
        user.setAge(-1);
    }

    @Override
    public void setAge(int age) {
        user.setAge(age);
    }

    @Override
    public void setAvatarFromCamera(int REQUEST_CODE) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = ((EditProfileActivity) getView()).getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image;
        try {
            image = File.createTempFile(imageFileName,".jpg",storageDir);
            photoPath = image.getAbsolutePath();
        }catch (IOException e){
            e.printStackTrace();
            getView().showMessage(R.string.exeptionError);
            return;
        }
        Uri photoURI = GenericFileProvider.getUriForFile(((EditProfileActivity) getView()),
                "ru.softwarecenter.pickupp.provider",
                image);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        ((AppCompatActivity) getView()).startActivityForResult(cameraIntent, REQUEST_CODE);
    }

    @Override
    public void setAvatarFromGallery(int REQUEST_CODE) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((AppCompatActivity) getView()).startActivityForResult(Intent.createChooser(intent, "Выберете фотографию"), REQUEST_CODE);
    }

    public boolean checkPermission(){
        AppCompatActivity activity = (EditProfileActivity) getView();
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE) ){
                getView().showPermissionMessage();
            }else{
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_STORAGE_REQUEST_CODE);
            }
            return false;
        }else{
            return true;
        }
    }




    @Override
    public void generateBitmap(String path) {
        if(path == null) {
            if (photoPath != null) {
                Bitmap bitmap = ImageUtils.rotateIfNeed(photoPath);
                user.setAvatar(ImageUtils.generateBase64Output(bitmap));
                getView().setAvatarImage(bitmap);
                photoPath = null;
            } else {
                getView().showMessage(R.string.exeptionError);
            }
        }else{
            Bitmap bitmap = ImageUtils.rotateIfNeed(path);
            user.setAvatar(ImageUtils.generateBase64Output(bitmap));
            getView().setAvatarImage(bitmap);
        }
    }

    @Override
    public void enterBtnClick() {
        /*if (!isAvatarAdded()) {
            getView().showMessage(R.string.avatarError);
        } else*/
        if (!isSexAdded) {
            getView().showMessage(R.string.sexError);
        } else if (!getView().isNameAdded()) {
            getView().showMessage(R.string.nameError);
        } else if (!getView().isAgeAdded()) {
            getView().showMessage(R.string.ageError);
        } else {
            getView().onBackScreen();
        }
    }



    @Override
    public void removeAvatar() {
        getView().removeAvatar();
        user.setAvatar(null);
    }

    @Override
    public void saveData() {

    }

    @Override
    public void skipSaveData() {

    }
}
