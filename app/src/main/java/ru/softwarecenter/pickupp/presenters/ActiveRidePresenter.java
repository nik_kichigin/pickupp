package ru.softwarecenter.pickupp.presenters;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.ActiveRideTravelDetailsAdapter;
import ru.softwarecenter.pickupp.interfaces.ActiveRideInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;

public class ActiveRidePresenter extends BasePresenter<ActiveRideInterface.View> implements ActiveRideInterface.Presenter{

    private List<String> travelDetailsListTitle;
    private HashMap<String, List<String>> travelDetailsListDetail;
    private List<String> travelDetailsDetailsList;

    private ActiveRideTravelDetailsAdapter adapterTravelDetails;

    @Override
    public void initTravelDetailsArrays() {
        travelDetailsListTitle = new ArrayList<>();
        travelDetailsDetailsList = new ArrayList<>();
        travelDetailsListDetail = new HashMap<>();
        travelDetailsListTitle.add("Детали поездки");
        travelDetailsDetailsList.add("");
    }

    @Override
    public void getAdapterTravelDetails(Context context) {
        travelDetailsListDetail.clear();
        travelDetailsListDetail.put(travelDetailsListTitle.get(0), travelDetailsDetailsList);
        adapterTravelDetails = new ActiveRideTravelDetailsAdapter(context, travelDetailsListTitle, travelDetailsListDetail);
        getView().setTravelDetailsListView(adapterTravelDetails);
    }
}
