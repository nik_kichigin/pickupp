package ru.softwarecenter.pickupp.presenters;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.ChangeNumberInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;

public class ChangeNumberPresenter extends BasePresenter<ChangeNumberInterface.View> implements ChangeNumberInterface.Presenter{

    private String phone;
    private String code;

    @Override
    public void phoneFiled(String phone) {
        if(phone.length() == 10){
            getView().updateButtonSendPhone();
            getView().setFocusOnCodeField();
        }else{
            getView().showMessage(R.string.phoneNumberLenghError);
        }
    }

    @Override
    public void codeFiled(String code, String phone) {
        if(phone.length() == 10){
            if(code.length() == 4){
                savePhoneAndCode(code, phone);
            }else{
                getView().showMessage(R.string.codeLengthError);
            }
        }else{
            getView().showMessage(R.string.phoneNumberLenghError);
        }

    }

    private void savePhoneAndCode(String code, String phone) {
        this.code = code;
        this.phone = phone;
    }
}
