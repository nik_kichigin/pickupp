package ru.softwarecenter.pickupp.presenters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMessagesAdapter;
import ru.softwarecenter.pickupp.interfaces.ChatActiveRideInterface;
import ru.softwarecenter.pickupp.interfaces.base.BasePresenter;
import ru.softwarecenter.pickupp.models.expandable_list_models.Message;

public class ChatActiveRidePresenter extends BasePresenter<ChatActiveRideInterface.View> implements ChatActiveRideInterface.Presenter{

    RecyclerView recyclerView;
    Context context;
    List<Message> messagesList = new ArrayList<>();
    RVMessagesAdapter adapter;

    @Override
    public void initRecyclerView(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;

        Message messageSys = new Message();
        messageSys.setOwner_user_message("system_message");
        messageSys.setMessage("Предлагаемое место посадки: ул.Науки, 14 в 12:45");
        messageSys.setTime("09:07");
        messagesList.add(messageSys);

        Message message = new Message();
        message.setOwner_user_message("another_message");
        message.setMessage("Куда подъехать чтобы вас встретить?");
        message.setTime("09:11");
        messagesList.add(message);

        adapter = new RVMessagesAdapter(context, recyclerView, messagesList);
        getView().setAdapterToRV(adapter);


    }

    @Override
    public void sendMessage(String messageText) {
        Message message = new Message();
        message.setOwner_user_message("my_message");
        message.setMessage(messageText);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sf = new SimpleDateFormat("HH:mm", Locale.GERMANY);
        message.setTime(sf.format(date));
        messagesList.add(message);

        adapter.notifyItemInserted(messagesList.size() - 1);
        adapter.scrollToLastItemPermoment();
    }
}
