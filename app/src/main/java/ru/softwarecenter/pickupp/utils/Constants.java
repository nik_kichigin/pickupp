package ru.softwarecenter.pickupp.utils;


public class Constants {
    public final static int MAP_STATE_SEARCH_FROM = 0;
    public final static int MAP_STATE_SEARCH_TO = 1;
    public final static int MAP_STATE_RIDE = 2;
    public final static long ONE_MINUTE_IN_MS = 60000;
    public final static int OPEN_SEARCH_RIDE_SCREEN_REQUEST_CODE = 17;
    public static final int RESULT_CANCELED = 3;
    public static final int RESULT_OK = 4;
}
