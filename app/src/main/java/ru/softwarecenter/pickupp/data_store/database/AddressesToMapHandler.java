package ru.softwarecenter.pickupp.data_store.database;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AddressesToMapHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "adressesToMapManager";
    private static final String TABLE_ADDRESSES_TO = "addresses";
    private static final String KEY_ID = "id";
    private static final String KEY_CITY = "city";
    private static final String KEY_COUNTRY = "country";
    private static final String KEY_HOUSE = "house";
    private static final String KEY_STREET = "street";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";


    public AddressesToMapHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HISTORY_REQUESTS_TABLE = "CREATE TABLE " + TABLE_ADDRESSES_TO + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_HOUSE + " TEXT," + KEY_STREET + " TEXT," + KEY_CITY
                + " TEXT," + KEY_COUNTRY + " TEXT," + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT" + ")";
        db.execSQL(CREATE_HISTORY_REQUESTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESSES_TO);

        onCreate(db);
    }

    public void addAddress(String house, String street, String city, String contry, double lat, double lng) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(KEY_ID, );
        values.put(KEY_HOUSE, house);
        values.put(KEY_STREET, street);
        values.put(KEY_CITY, city);
        values.put(KEY_COUNTRY, contry);
        values.put(KEY_LATITUDE, lat);
        values.put(KEY_LONGITUDE, lng);

        db.insert(TABLE_ADDRESSES_TO, null, values);
        db.close();

        deleteLastRow();
    }

    private void deleteLastRow() {
        String selectQuery = "SELECT  * FROM " + TABLE_ADDRESSES_TO;

        SQLiteDatabase db = this.getWritableDatabase();
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);

        int i = 0;
        int id = 0;
        if (cursor.moveToFirst()) {
            id = Integer.parseInt(cursor.getString(0));
            do {
                i ++;
            } while (cursor.moveToNext());
        }

        if (i >= 11) {
            db.delete(TABLE_ADDRESSES_TO, KEY_ID + "=" + id, null);
        }
    }

    public List<Address> getAllAddresses() {
        List<Address> addressesList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_ADDRESSES_TO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Address address = new Address(null);

                address.setSubThoroughfare(cursor.getString(1));
                address.setThoroughfare(cursor.getString(2));
                address.setLocality(cursor.getString(3));
                address.setCountryName(cursor.getString(4));
                address.setLatitude(cursor.getDouble(5));
                address.setLongitude(cursor.getDouble(6));

                addressesList.add(address);
            } while (cursor.moveToNext());
        }

        return addressesList;
    }

    public void deleteAllAddresses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ADDRESSES_TO, null, null);
        db.close();
    }
}
