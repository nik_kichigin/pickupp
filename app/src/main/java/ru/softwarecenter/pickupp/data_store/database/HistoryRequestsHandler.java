package ru.softwarecenter.pickupp.data_store.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;

public class HistoryRequestsHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "historyRidesManager";
    private static final String TABLE_HISTORY_REQUESTS = "requests";
    private static final String KEY_ID = "id";
    private static final String KEY_TIME = "time";
    private static final String KEY_FROM_ADRESS = "from_adress";
    private static final String KEY_TO_ADRESS = "to_adress";


    public HistoryRequestsHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HISTORY_REQUESTS_TABLE = "CREATE TABLE " + TABLE_HISTORY_REQUESTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TIME + " TEXT," +
                KEY_FROM_ADRESS + " TEXT," + KEY_TO_ADRESS + " TEXT" + ")";
        db.execSQL(CREATE_HISTORY_REQUESTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY_REQUESTS);

        onCreate(db);
    }

    public void addRequests(int id, long time, String from_adress, String to_adress) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, id);
        values.put(KEY_TIME, time);
        values.put(KEY_FROM_ADRESS, from_adress);
        values.put(KEY_TO_ADRESS, to_adress);

        db.insert(TABLE_HISTORY_REQUESTS, null, values);
        db.close();
    }

    public List<Ride> getAllHistoryRequests() {
        List<Ride> historyRidesList = new ArrayList<Ride>();
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY_REQUESTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Ride ride = new Ride();
                Street from = new Street();
                Street to = new Street();
                ride.setTime(Long.valueOf(cursor.getString(1)));
                from.setAdress(cursor.getString(2));
                to.setAdress(cursor.getString(3));
                ride.setFrom(from);
                ride.setTo(to);

                historyRidesList.add(ride);
            } while (cursor.moveToNext());
        }

        return historyRidesList;
    }

    public void deleteAllImages() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY_REQUESTS, null, null);
        db.close();
    }
}
