package ru.softwarecenter.pickupp.interfaces;

import android.app.Activity;

import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.TravelDetailsAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface PreviousRideInterface {

    interface View extends BaseInterfaceView {
        //void setAboutTravelerListView(ActiveRideTravelDetailsAdapter adapter);
        void setTravelDetailsListView(TravelDetailsAdapter adapter);
    }

    interface Presenter extends BaseInterfacePresenter<PreviousRideInterface.View> {
        //void initAboutTravelerArrays();
        void initTravelDetailsArrays();
        //void getAdapterAboutTraveler(Activity activity);
        void getAdapterTravelDetails(Activity activity);
    }

}
