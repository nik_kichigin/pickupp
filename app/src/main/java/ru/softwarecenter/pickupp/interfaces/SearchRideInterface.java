package ru.softwarecenter.pickupp.interfaces;

import android.widget.TextView;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;
import ru.softwarecenter.pickupp.models.Ride;

public interface SearchRideInterface {

    interface View extends BaseInterfaceView{
        void updateView(Ride ride);
        void openAdditionalInfo();
        void closeAdditionalInfo();
        void changeAdresses(String from, String to);
        void setRadiusAfter(String radius);
        void cleanRadiusAfter();
        void setRadiusBefore(String radius);
        void cleanRadiusBefore();
        void setCarActive();
        void setTaxiActive();
        void setBagActive();
        void setBagDisable();
        void setCarAndTaxiDisable();
        void openSearchDetailsMap(Ride ride, String statePoint);
        void backToMapScreen(Ride ride);
        void openNumberPickerDialog(TextView timeText);
        void openTimeDepartDialog(TextView textView);
        void showSearchRecyclerView();
        void updateToolbarTextFields(String startTime, String earlyLateTimeText);
    }

    interface Presenter extends BaseInterfacePresenter<View>{
        void setRide(Ride ride);
        void addressFromCilck();
        void addressToCilck();
        void changeAdressClick();
        void radiusAfterClick();
        void radiusBeforeClick();
        void additionalClick();
        void searchChose(int isPickUpp);
        void costAdded(String s);
        void peopleAdded(String s);
        void bagClicked();
        void setAddressText();
        void onLetsRideBtnClick();
        void openTimeDepartureDialog();

        Ride getRide();

    }
}
