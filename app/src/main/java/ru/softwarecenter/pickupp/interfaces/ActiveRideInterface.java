package ru.softwarecenter.pickupp.interfaces;

import android.content.Context;

import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.ActiveRideTravelDetailsAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface ActiveRideInterface {

    interface View extends BaseInterfaceView {
        void setTravelDetailsListView(ActiveRideTravelDetailsAdapter adapter);
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void initTravelDetailsArrays();
        void getAdapterTravelDetails(Context context);
    }

}
