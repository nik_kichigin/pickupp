package ru.softwarecenter.pickupp.interfaces;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface MyProfileInterface {

    interface View extends BaseInterfaceView {
        String getStatusText();
        String getAboutYourselfText();
        void setStatusToUI(String statusText);
        void setAboutYourselfToUI(String aboutYourselfText);
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void saveStatusClick();
        void saveAboutYourselfClick();
    }
}
