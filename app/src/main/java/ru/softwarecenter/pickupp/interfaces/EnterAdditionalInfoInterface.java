package ru.softwarecenter.pickupp.interfaces;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface EnterAdditionalInfoInterface {

    interface View extends BaseInterfaceView{
        void showMessage(int messageResId);
        void showPermissionMessage();
        void openSettingsForGrantPermission(int REQUEST_CODE);
        void enterBtnClick();
        void onMapScreen();
        void setManSexSelected();
        void setWomanSexSelected();
        void setSexDeselected();
        void setAvatarImage(Bitmap bitmap);
        void removeAvatar();
        boolean isNameAdded();
        boolean isAgeAdded();
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void setSex(int sex);
        void setName(String name);
        void clearName();
        void setAge(int age);
        void clearAge();
        void setAvatarFromCamera(int REQUEST_CODE);
        void setAvatarFromGallery(int REQUEST_CODE);
        void generateBitmap(String path);
        void removeAvatar();
        void enterBtnClick();
        void setStringAvatar(String string);
        void saveData();
        void skipSaveData();
    }
}
