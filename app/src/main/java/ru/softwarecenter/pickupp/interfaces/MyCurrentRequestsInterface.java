package ru.softwarecenter.pickupp.interfaces;

import android.app.Activity;

import java.util.List;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMyCurrentRequestsAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;
import ru.softwarecenter.pickupp.models.Ride;

public interface MyCurrentRequestsInterface {

    interface View extends BaseInterfaceView {

    }

    interface Presenter extends BaseInterfacePresenter<View> {
        RVMyCurrentRequestsAdapter getMyCurrentRequestsAdapter();
    }

}
