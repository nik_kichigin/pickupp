package ru.softwarecenter.pickupp.interfaces;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVHistoryRequestsAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;
import ru.softwarecenter.pickupp.models.Ride;

public interface HistoryRequestsInterface {
    interface View extends BaseInterfaceView {
        RecyclerView getRecyclerView();
        void setAdapterToRV(RVHistoryRequestsAdapter adapter);
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void initRecyclerView(Context context);
    }
}
