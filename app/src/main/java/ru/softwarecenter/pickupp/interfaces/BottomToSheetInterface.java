package ru.softwarecenter.pickupp.interfaces;

import ru.softwarecenter.pickupp.models.Street;

public interface BottomToSheetInterface {
    void onCloseClick();
    void onItemClick(Street street);
    void onMapClick();
    void onFromViewClick();
}
