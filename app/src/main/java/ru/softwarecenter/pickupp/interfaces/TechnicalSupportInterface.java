package ru.softwarecenter.pickupp.interfaces;


import android.app.Activity;

import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.CategorySupportAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface TechnicalSupportInterface {

    interface View extends BaseInterfaceView {
        void setCathegoryListView(CategorySupportAdapter adapter);
        void setPathComunicateListView(CategorySupportAdapter adapter);
        void showMessage(int messageResId);
        String getEmailText();
        String getMessageText();
        void sendMessage();
    }

    interface Presenter extends BaseInterfacePresenter<TechnicalSupportInterface.View> {
        void initCategoryArrays();
        void initPathComunicateArrays();
        void getAdapterCathegory(Activity activity);
        void getAdapterPathComunicate(Activity activity);
        void onCategoryChildItemClick(int groupPosition, int childPosition);
        void onPathComunicateChildItemClick(int groupPosition, int childPosition);
        void setEmailFieldOpenState(boolean isEmailFieldOpen);
        void onSendMessageBtnClick();
    }
}
