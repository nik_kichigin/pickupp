package ru.softwarecenter.pickupp.interfaces;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface RatingRideInterface {

    interface View extends BaseInterfaceView {

    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void setRatingValue(float value);
    }

}
