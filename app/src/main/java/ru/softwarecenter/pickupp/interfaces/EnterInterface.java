package ru.softwarecenter.pickupp.interfaces;


import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface EnterInterface {

    interface View extends BaseInterfaceView{
        void updateButtonSendPhone();
        void setFocusOnCodeField();
        void showMessage(int messageResId);
        void openEnterAdditionalInfoScreen();
        void openMapScreen();
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void phoneFiled(String phone);
        void codeFiled(String code, String phone);
    }
}
