package ru.softwarecenter.pickupp.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}
