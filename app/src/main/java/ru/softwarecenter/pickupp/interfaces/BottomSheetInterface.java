package ru.softwarecenter.pickupp.interfaces;

import ru.softwarecenter.pickupp.models.Street;

public interface BottomSheetInterface {
    void onCloseClick();
    void onItemClick(Street street);
    void onMapClick();
}
