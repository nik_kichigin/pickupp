package ru.softwarecenter.pickupp.interfaces;

import ru.softwarecenter.pickupp.models.Ride;

public interface SearchRideItemClick {
    void itemClick(Ride ride);
}
