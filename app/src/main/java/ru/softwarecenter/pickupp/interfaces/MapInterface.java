package ru.softwarecenter.pickupp.interfaces;

import android.content.Context;
import android.location.Address;
import android.net.Uri;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;
import ru.softwarecenter.pickupp.models.Ride;


public interface MapInterface{

    interface View extends BaseInterfaceView{
        void updateTitle(String title);
        void hideUI();
        void showUI();
        void showMessage(int messageResId);
        void showPermissionMessage();
        void openSettingsForGrantPermission(int REQUEST_CODE);
        void driverBtnEnabledFrom();
        void driverBtnEnabledTo();
        void driverBtnDisabled();
        void openFromBottomSheet();
        void openToBottomSheet();
        void changeViewStateTo();
        void changeViewStateFrom();
        void openSearchScreen(Ride ride);
        void closeMenu();
        void closeApp();
        boolean isMenuOpen();

        SupportMapFragment getMapFragment();
    }

    interface Presenter extends BaseInterfacePresenter<View>{
        void setGoogleApiClient(GoogleApiClient googleApiClient);
        void driverClick();
        void locationClick();
        void openSheet();
        void searhBtnClick();
        void onFromViewClick();
        boolean isRideDriverOn();
        void onAddressFromCardClick(Address address);
        void onAddressToCardClick(Address address);
        void setMapGestures(boolean isMenuOpened);
        void onBackPressed();
        void backToMapEvent(int requestCode, Ride ride);
        void setTimeRide(String time);

        String getFromText();
        List<Address> getAdresses(String adress, Context context);
    }
}
