package ru.softwarecenter.pickupp.interfaces;

public interface SearchItemBottomSheetInterface {
    void onCloseClick();
    void onLetsRideClick();
}
