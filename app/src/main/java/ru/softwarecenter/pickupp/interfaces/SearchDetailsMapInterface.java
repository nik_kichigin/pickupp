package ru.softwarecenter.pickupp.interfaces;

import android.content.Context;
import android.location.Address;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.presenters.SearchRidePresenter;
import ru.softwarecenter.pickupp.views.SearchRideActivity;

public interface SearchDetailsMapInterface {

    interface View extends BaseInterfaceView {
        void updateTitle(String title);
        void hideUI();
        void showUI();
        void showMessage(int messageResId);
        void showPermissionMessage();
        void openSettingsForGrantPermission(int REQUEST_CODE);
        void openFromBottomSheet();
        void openToBottomSheet();
        void setViewStateTo();
        void setViewStateFrom();
        void backToSearchScreen(Ride ride);

        SupportMapFragment getMapFragment();
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void setGoogleApiClient(GoogleApiClient googleApiClient);
        void openSheet();
        void bottomBackBtnClick();
        void onAddressFromCardClick(Address address);
        void onAddressToCardClick(Address address);
        void setRide(Ride ride);
        void setCurrentMapState(String mapState);
        Ride getRide();

        List<Address> getAdresses(String adress, Context context);
    }

}
