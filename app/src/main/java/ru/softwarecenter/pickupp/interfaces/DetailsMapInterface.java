package ru.softwarecenter.pickupp.interfaces;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.SupportMapFragment;

import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface DetailsMapInterface {

    interface View extends BaseInterfaceView {
        void updateTitle(String title);
        void hideUI();
        void showUI();
        void showPermissionMessage();
        void openSettingsForGrantPermission(int REQUEST_CODE);

        String getMarkerState();
        SupportMapFragment getMapFragment();
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void locationClick();
        void setGoogleApiClient(GoogleApiClient googleApiClient);


    }

}
