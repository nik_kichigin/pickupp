package ru.softwarecenter.pickupp.interfaces;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMessagesAdapter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfacePresenter;
import ru.softwarecenter.pickupp.interfaces.base.BaseInterfaceView;

public interface ChatActiveRideInterface {

    interface View extends BaseInterfaceView {
        void setAdapterToRV(RVMessagesAdapter adapter);
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void initRecyclerView(Context context, RecyclerView recyclerView);
        void sendMessage(String message);
    }

}
