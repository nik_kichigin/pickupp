package ru.softwarecenter.pickupp.views;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.SupportMapFragment;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.callback.DragStateListener;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.BottomSheetInterface;
import ru.softwarecenter.pickupp.interfaces.BottomToSheetInterface;
import ru.softwarecenter.pickupp.interfaces.MapInterface;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.presenters.MapPresenter;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.fragments.DateDialog;
import ru.softwarecenter.pickupp.views.fragments.MapBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.MapToBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.TimeDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static ru.softwarecenter.pickupp.presenters.MapPresenter.PERMISSION_LOCATION_REQUEST_CODE;


public class MapActivity extends AppCompatActivity implements MapInterface.View {

    private MapPresenter presenter;
    private SlidingRootNav slidingRootNav;

    @BindView(R.id.adress)
    TextView adress;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.changeAdress)
    TextView changeAdress;

    @BindView(R.id.buble)
    ImageView buble;

    @BindView(R.id.locationBtn)
    ImageView locationBtn;

    @BindView(R.id.glass_icon)
    ImageView glassIcon;

    @BindView(R.id.driverTxt)
    TextView driverTxt;

    @BindView(R.id.time_text)
    TextView timeTxt;

    @BindView(R.id.date_text)
    TextView dateTxt;

    @BindView(R.id.select_time)
    LinearLayout selectTime;

    @BindView(R.id.bottom_button_text)
    TextView bottomTxt;

    @BindView(R.id.driverBtn)
    CardView driverBtn;

    @BindView(R.id.searchBtn)
    CardView searchBtn;

    @BindView(R.id.driverColor)
    View driverView;

    @BindView(R.id.menu)
    ImageButton menu;

    @BindView(R.id.rootView)
    ConstraintLayout rootView;

    @BindView(R.id.block_layout)
    View blockView;

    MapBottomSheetDialogFragment sheetFromFragment;
    MapToBottomSheetDialogFragment sheetToFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .withDragDistance(290) //Horizontal translation of a view. Default == 180dp
                .withRootViewScale(1f) //Content view's scale will be interpolated between 1f and 0.7f. Default == 0.65f;
                .withRootViewElevation(5) //Content view's elevation will be interpolated between 0 and 10dp. Default == 8.
                .withRootViewYTranslation(0) //Content view's translationY will be interpolated between 0 and 4. Default == 0
                .withContentClickableWhenMenuOpened(true)
                .addDragStateListener(new DragStateListener() {
                    @Override
                    public void onDragStart() {
                    }

                    @Override
                    public void onDragEnd(boolean isMenuOpened) {
                        presenter.setMapGestures(isMenuOpened);
                    }
                })
                .inject();

        ButterKnife.bind(this);
        timeTxt.setText(new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS));
        initPresenter();
        attachView();
        //Crashlytics.getInstance().crash();
    }

    private void initPresenter(){
        presenter = new MapPresenter();
        presenter.attachView(this);
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, presenter)
                .addConnectionCallbacks(presenter)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
        presenter.setGoogleApiClient(mGoogleApiClient);
    }

    @OnClick(R.id.menu)
    void menuIconClick() {
        slidingRootNav.openMenu();
    }

    @OnClick(R.id.id_nav_my_profile)
    void avatarImageClick() {
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.id_nav_map)
    void mapItemClick() {
        slidingRootNav.closeMenu();
    }

    @OnClick(R.id.id_nav_requests)
    void requestsItemClick() {
        Intent intent = new Intent(this, MyCurrentRequestsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.id_nav_history)
    void historyItemClick() {
        Intent intent = new Intent(this, HistoryRequestsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.id_nav_support)
    void supportItemClick() {
        Intent intent = new Intent(this, TechnicalSupportActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.id_nav_about_app)
    void aboutAppItemClick() {
        Intent intent = new Intent(this, AboutApplicationActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @Override
    public void updateTitle(String title) {
        adress.setText(title);
        if(!title.equals("")) {
            YoYo.with(Techniques.FadeIn).duration(100).playOn(adress);
            YoYo.with(Techniques.FadeIn).duration(100).playOn(changeAdress);
        }
    }

    @Override
    public void hideUI() {
        YoYo.with(Techniques.FadeOut).duration(100).playOn(adress);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(title);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(menu);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(driverBtn);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(changeAdress);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(searchBtn);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(locationBtn);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(selectTime);
    }

    @Override
    public void showUI() {
        YoYo.with(Techniques.FadeIn).duration(100).playOn(title);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(menu);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(driverBtn);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(searchBtn);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(locationBtn);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(changeAdress);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(selectTime);
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @OnClick(R.id.driverBtn)
    public void driverCilck(){
        presenter.driverClick();
    }

    @OnClick(R.id.searchBtn)
    public void searchClick(){
        presenter.searhBtnClick();
    }

    @OnClick(R.id.changeAdress)
    public void changeAdressClick(){
        presenter.openSheet();
    }

    @OnClick(R.id.locationBtn)
    public void locationClick(){
        presenter.locationClick();
    }

    @Override
    public void driverBtnEnabledFrom() {
        driverView.setBackgroundResource(R.drawable.shape_driver_btn_primary_background);
        driverTxt.setTextColor(Color.WHITE);
    }

    @Override
    public void driverBtnEnabledTo() {
        driverView.setBackgroundResource(R.drawable.shape_driver_btn_accent_background);
        driverTxt.setTextColor(Color.WHITE);
    }

    @Override
    public void driverBtnDisabled() {
        driverView.setBackgroundResource(R.drawable.shape_map_buttons_background);
        driverTxt.setTextColor(Color.BLACK);
    }

    @OnClick(R.id.select_time)
    void selectTimeClick() {
        openTimeDateSelectorDialog();
    }

    private void openTimeDateSelectorDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Изменить дату или время поездки?");

        builder.setPositiveButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Дата", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DateDialog dateDialog = new DateDialog(dateTxt);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                dateDialog.show(fragmentTransaction, "Dateicker");
            }
        });

        builder.setNegativeButton("Время", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                TimeDialog timeDialog = new TimeDialog(timeTxt);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                timeDialog.show(fragmentTransaction, "TimePicker");
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

        final Button cancelBtn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        cancelBtn.setTextColor(getResources().getColor(R.color.colorPrimary));

        final Button timeBtn = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        timeBtn.setTextColor(getResources().getColor(R.color.colorPrimary));

        final Button dateBtn = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        dateBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void showPermissionMessage() {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtn, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(MapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_LOCATION_REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public void openSettingsForGrantPermission(final int REQUEST_CODE) {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtnSettings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(appSettingsIntent, REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public void openFromBottomSheet() {
        MapBottomSheetDialogFragment bottomSheetDialogFragment = new MapBottomSheetDialogFragment( this);
        sheetFromFragment = bottomSheetDialogFragment;

        bottomSheetDialogFragment.setSheetInterface(new BottomSheetInterface() {
            @Override
            public void onCloseClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onItemClick(Street street) {

            }

            @Override
            public void onMapClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }
        });
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void openToBottomSheet() {
        MapToBottomSheetDialogFragment bottomSheetDialogFragment = new MapToBottomSheetDialogFragment(this);
        sheetToFragment = bottomSheetDialogFragment;
        bottomSheetDialogFragment.setSheetInterface(new BottomToSheetInterface() {
            @Override
            public void onCloseClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onItemClick(Street street) {

            }

            @Override
            public void onMapClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onFromViewClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
                presenter.onFromViewClick();
            }


        });
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void changeViewStateTo() {
        buble.setImageResource(R.drawable.buble_to);
        locationBtn.setImageResource(R.drawable.location_red);
        glassIcon.setImageResource(R.drawable.glass_red);
        timeTxt.setTextColor(getResources().getColor(R.color.colorAccent));
        dateTxt.setTextColor(getResources().getColor(R.color.colorAccent));
        title.setText("Куда:");
        bottomTxt.setText("Найти варианты");
        if (presenter.isRideDriverOn()) {
            driverBtnEnabledTo();
        }
    }

    @Override
    public void changeViewStateFrom() {
        buble.setImageResource(R.drawable.buble_from);
        locationBtn.setImageResource(R.drawable.location_blue);
        glassIcon.setImageResource(R.drawable.glass_blue);
        timeTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
        dateTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
        title.setText("Откуда:");
        bottomTxt.setText("Куда?");
        if (presenter.isRideDriverOn()) {
            driverBtnEnabledFrom();
        }

    }

    public MapInterface.Presenter getPresenter(){
        if(presenter != null){
            return presenter;
        }else{
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            return;
        }
        if(requestCode == PERMISSION_LOCATION_REQUEST_CODE){
            if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                presenter.updateLocation();
            }else{
                openSettingsForGrantPermission(PERMISSION_LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public SupportMapFragment getMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public void openSearchScreen(Ride ride) {
        presenter.setTimeRide(timeTxt.getText().toString());
        Intent intent = new Intent(this, SearchRideActivity.class);
        intent.putExtra(Ride.class.getCanonicalName(), ride);

        startActivityForResult(intent, Constants.OPEN_SEARCH_RIDE_SCREEN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            return;
        }
        if(requestCode == Constants.OPEN_SEARCH_RIDE_SCREEN_REQUEST_CODE) {
            Ride ride = data.getParcelableExtra(Ride.class.getCanonicalName());
            presenter.backToMapEvent(resultCode, ride);

            if (resultCode == Constants.RESULT_OK) {
                Intent intent = new Intent(this, ChatActiveRideActivity.class);
                startActivity(intent);
            } else if (resultCode == Constants.RESULT_CANCELED) {
                timeTxt.setText(new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS));
            }
        }


    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    public void closeMenu() {
        slidingRootNav.closeMenu();
    }

    @Override
    public void closeApp() {
        super.onBackPressed();
    }

    @Override
    public boolean isMenuOpen() {
        return slidingRootNav.isMenuOpened();
    }

    public void onAdressFromCardClick(View view) {
        Address address = (Address) view.getTag();
        presenter.onAddressFromCardClick(address);

        sheetFromFragment.dismiss();
    }

    public void onAdressToCardClick(View view) {
        Address address = (Address) view.getTag();
        presenter.onAddressToCardClick(address);

        sheetToFragment.dismiss();
    }
}
