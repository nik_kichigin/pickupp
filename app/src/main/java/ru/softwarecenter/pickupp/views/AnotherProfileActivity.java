package ru.softwarecenter.pickupp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.AppCompatRatingBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;

public class AnotherProfileActivity extends AppCompatActivity {

    @BindView(R.id.scaleRatingBar)
    ScaleRatingBar rb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.another_profile_layout);

        ButterKnife.bind(this);

        rb = new ScaleRatingBar(this);

        rb.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(BaseRatingBar baseRatingBar, float v) {
                Toast.makeText(getApplicationContext(), String.valueOf(v), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @OnClick(R.id.back_btn)
    void backButtonClick() {
        onBackPressed();
    }
}
