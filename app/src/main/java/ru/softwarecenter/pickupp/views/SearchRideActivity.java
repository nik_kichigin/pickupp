package ru.softwarecenter.pickupp.views;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVSearchAdapter;
import ru.softwarecenter.pickupp.interfaces.SearchItemBottomSheetInterface;
import ru.softwarecenter.pickupp.interfaces.SearchRideInterface;
import ru.softwarecenter.pickupp.interfaces.SearchRideItemClick;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.presenters.SearchRidePresenter;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.utils.ResizeAnimUtil;
import ru.softwarecenter.pickupp.views.fragments.SearchItemBottomSheetDialog;
import ru.softwarecenter.pickupp.views.fragments.TimeDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchRideActivity extends AppCompatActivity implements SearchRideInterface.View{

    @BindView(R.id.hideableLayout)
    LinearLayout hideableLayout;

    @BindView(R.id.search_car_layout)
    LinearLayout searchCarLayout;

    @BindView(R.id.addressFrom)
    TextView addressFrom;

    @BindView(R.id.addressTo)
    TextView addressTo;

    @BindView(R.id.radiusBeforeRide)
    EditText radiusBefore;

    @BindView(R.id.radiusAfterRide)
    EditText radiusAfter;

    @BindView(R.id.coastRideText)
    EditText coastRideText;

    @BindView(R.id.peopleSizeRideText)
    EditText peopleSizeRideText;

    @BindView(R.id.metersAfterRide)
    TextView metersAfterRide;

    @BindView(R.id.metersBeforeRide)
    TextView metersBeforeRide;

    @BindView(R.id.currencyCoastRide)
    TextView currencyCoastRide;

    @BindView(R.id.peopleSizeRide)
    TextView peopleSizeRide;

    @BindView(R.id.additional_text)
    TextView additionalText;

    @BindView(R.id.additional_icon)
    ImageView additionalIcon;

    @BindView(R.id.carBtn)
    CardView carCard;

    @BindView(R.id.carTxt)
    TextView carTxt;

    @BindView(R.id.taxiBtn)
    CardView taxiCard;

    @BindView(R.id.taxiTxt)
    TextView taxiTxt;

    @BindView(R.id.ride_start_time_text_toolbar)
    TextView startTimeTextToolbar;

    @BindView(R.id.ride_early_late_time_text_toolbar)
    TextView earlyLateTimeTextToolbar;

    @BindView(R.id.bagBtn)
    CardView bagCard;

    @BindView(R.id.bagTxt)
    TextView bagTxt;

    @BindView(R.id.id_bottom_text)
    TextView bottomText;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    RVSearchAdapter adapter = null;
    private SearchRidePresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_ride_layout);
        ButterKnife.bind(this);

        initPresenter();
        initUIFields();
        closeAdditionalInfo();
        attachView();
    }

    private void initPresenter() {
        presenter = new SearchRidePresenter();
        presenter.attachView(this);
    }

    private void setSearchAdapterToRV() {
        adapter = new RVSearchAdapter(this, recyclerView, new SearchRideItemClick() {
            @Override
            public void itemClick(Ride ride) {
                SearchItemBottomSheetDialog bottomSheetDialogFragment = new SearchItemBottomSheetDialog(SearchRideActivity.this);
                bottomSheetDialogFragment.setSheetInterface(new SearchItemBottomSheetInterface() {
                    @Override
                    public void onCloseClick() {
                        bottomSheetDialogFragment.dismiss();
                    }

                    @Override
                    public void onLetsRideClick() {
                        bottomSheetDialogFragment.dismiss();
                        presenter.onLetsRideBtnClick();
                    }
                });
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setFocusable(false);
        for(int i = 0; i < 20; i++){
            Ride ride = new Ride();
            ride.setName("item " + i);
            adapter.addItem(ride);
        }
        ((NestedScrollView)findViewById(R.id.scrollView)).scrollTo(0, 0);
    }

    private void initUIFields() {
        Intent intent = getIntent();
        Ride ride = intent.getParcelableExtra(Ride.class.getCanonicalName());
        addressFrom.setText(ride.getFrom().getAdress());
        addressTo.setText(ride.getTo().getAdress());
        presenter.setRide(ride);
        if (ride.isDriverOn()) {
            searchCarLayout.setVisibility(View.GONE);
            bottomText.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            setSearchAdapterToRV();
        } else {
            bottomText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        startTimeTextToolbar.setText(new java.text.SimpleDateFormat("HH:mm").format(ride.getTime()));



        radiusAfter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (radiusAfter.getText().toString().length() > 0) {
                    if (metersAfterRide.getVisibility() == View.GONE) {
                        metersAfterRide.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (metersAfterRide.getVisibility() == View.VISIBLE) {
                        metersAfterRide.setVisibility(View.GONE);
                    }
                }
            }
        });

        radiusBefore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (radiusBefore.getText().toString().length() > 0) {
                    if (metersBeforeRide.getVisibility() == View.GONE) {
                        metersBeforeRide.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (metersBeforeRide.getVisibility() == View.VISIBLE) {
                        metersBeforeRide.setVisibility(View.GONE);
                    }
                }
            }
        });

        coastRideText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (coastRideText.getText().toString().length() > 0) {
                    if (currencyCoastRide.getVisibility() == View.GONE) {
                        currencyCoastRide.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (currencyCoastRide.getVisibility() == View.VISIBLE) {
                        currencyCoastRide.setVisibility(View.GONE);
                    }
                }
            }
        });

        peopleSizeRideText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (peopleSizeRideText.getText().toString().length() > 0) {
                    if (peopleSizeRide.getVisibility() == View.GONE) {
                        peopleSizeRide.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (peopleSizeRide.getVisibility() == View.VISIBLE) {
                        peopleSizeRide.setVisibility(View.GONE);
                    }
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @Override
    public void backToMapScreen(Ride ride) {
        Intent intent = new Intent();
        intent.putExtra(Ride.class.getCanonicalName(), ride);

        setResult(Constants.RESULT_OK, intent);

        this.finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Ride.class.getCanonicalName(), presenter.getRide());

        setResult(Constants.RESULT_CANCELED, intent);

        super.onBackPressed();
    }

    @OnClick(R.id.back_btn)
    void backBtnClick(){
        onBackPressed();
    }

    @OnClick(R.id.open_time_departure_dialog)
    void openTimeDepartureDialog(){
        presenter.openTimeDepartureDialog();
    }

    @OnClick(R.id.addressFrom)
    void addressFromClick(){
        presenter.addressFromCilck();
    }

    @OnClick(R.id.addressTo)
    void addressToClick(){
        presenter.addressToCilck();
    }

    @OnClick(R.id.additional_btn)
    void additionalClick(){
        presenter.additionalClick();
    }

    @OnClick(R.id.changeAdress)
    void changeAdress(){
        presenter.changeAdressClick();
    }

    @OnClick(R.id.radiusAfterRideBtn)
    void radiusAfterClick(){
        presenter.radiusAfterClick();
    }

    @OnClick(R.id.radiusBeforeRideBtn)
    void radiusBeforeClick(){
        presenter.radiusBeforeClick();
    }

    @OnClick(R.id.bagBtn)
    void bagClick(){
        presenter.bagClicked();
    }

    @OnClick(R.id.carBtn)
    void carClick(){
        presenter.searchChose(1);
    }

    @OnClick(R.id.taxiBtn)
    void taxiClick(){
        presenter.searchChose(2);
    }

    @Override
    public void updateView(Ride ride) {

    }

    @Override
    public void openTimeDepartDialog(TextView textView) {
        TimeDialog timeDialog = new TimeDialog(textView);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        timeDialog.show(fragmentTransaction, "TimePicker");
    }

    @Override
    public void showSearchRecyclerView() {
        if (adapter == null) {
            setSearchAdapterToRV();
        }
        if (recyclerView.getVisibility() == View.GONE) {
            recyclerView.setVisibility(View.VISIBLE);
        }
        if (bottomText.getVisibility() == View.VISIBLE) {
            bottomText.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateToolbarTextFields(String startTime, String earlyLateTimeText) {
        startTimeTextToolbar.setText(startTime);
        earlyLateTimeTextToolbar.setText(earlyLateTimeText);
    }

    @Override
    public void openNumberPickerDialog(TextView timeText) {
        final NumberPicker numberPicker = new NumberPicker(this);

        int color = getResources().getColor(R.color.colorPrimary);

        setDividerColor(numberPicker, color);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(60);
        numberPicker.setValue(Integer.parseInt(timeText.getText().toString()));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выберите количество минут");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timeText.setText(String.valueOf(numberPicker.getValue()));
            }
        });

        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setView(numberPicker);
        final AlertDialog dialog = builder.create();
        dialog.show();

        final Button okBtn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        okBtn.setTextColor(getResources().getColor(R.color.colorPrimary));

        final Button cancelBtn = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        cancelBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void openAdditionalInfo() {
        ResizeAnimUtil.expand(hideableLayout);
        additionalText.setText(R.string.search_additional_dismiss);
        additionalIcon.setImageResource(R.drawable.arrow_up_bold);
    }

    @Override
    public void closeAdditionalInfo() {
        ResizeAnimUtil.collapse(hideableLayout);
        additionalText.setText(R.string.search_additional_show);
        additionalIcon.setImageResource(R.drawable.plus_white_bold);
    }

    @Override
    public void changeAdresses(String from, String to) {
        //Toast.makeText(getApplicationContext(), from, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), to, Toast.LENGTH_SHORT).show();

        addressFrom.setText(from);
        addressTo.setText(to);
    }

    @Override
    public void setRadiusAfter(String radius) {
        radiusAfter.setText(radius);
        radiusAfter.setAlpha(1f);
    }

    @Override
    public void cleanRadiusAfter() {
        radiusAfter.setText(R.string.radius_hint);
        radiusAfter.setAlpha(0.8f);
    }

    @Override
    public void setRadiusBefore(String radius) {
        radiusBefore.setText(radius);
        radiusBefore.setAlpha(1f);
    }

    @Override
    public void cleanRadiusBefore() {
        radiusBefore.setText(R.string.radius_hint);
        radiusBefore.setAlpha(0.8f);
    }

    @Override
    public void setCarActive() {
        carCard.setBackgroundResource(R.color.colorRed);
        carCard.setCardElevation(0f);
        carTxt.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    public void setTaxiActive() {
        taxiCard.setBackgroundResource(R.color.colorRed);
        taxiCard.setCardElevation(0f);
        taxiTxt.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    public void setBagActive() {
        bagCard.setBackgroundResource(R.color.colorRed);
        bagCard.setCardElevation(0f);
        bagTxt.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    public void setBagDisable() {
        bagCard.setBackgroundResource(R.color.colorWhite);
        bagCard.setCardElevation(4f);
        bagTxt.setTextColor(getResources().getColor(R.color.colorBlack));
    }

    @Override
    public void setCarAndTaxiDisable() {
        carCard.setBackgroundResource(R.color.colorWhite);
        carCard.setCardElevation(4f);
        taxiCard.setBackgroundResource(R.color.colorWhite);
        taxiCard.setCardElevation(4f);
        carTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        taxiTxt.setTextColor(getResources().getColor(R.color.colorBlack));
    }

    @Override
    public void openSearchDetailsMap(Ride ride, String statePoint) {
        Intent intent = new Intent(this, SearchDetailsMapActivity.class);
        intent.putExtra("statePoint", statePoint);

        intent.putExtra(Ride.class.getCanonicalName(), ride);

        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Ride ride = data.getParcelableExtra(Ride.class.getCanonicalName());
        presenter.setRide(ride);

        addressFrom.setText(ride.getFrom().getAdress());
        addressTo.setText(ride.getTo().getAdress());
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
