package ru.softwarecenter.pickupp.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.MyProfileInterface;
import ru.softwarecenter.pickupp.presenters.MyProfilePresenter;

public class MyProfileActivity extends AppCompatActivity implements MyProfileInterface.View, TextView.OnEditorActionListener {

    @BindView(R.id.status_text)
    EditText statusText;

    @BindView(R.id.about_yourself_text)
    EditText aboutYourselfText;

    MyProfilePresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_layout);

        ButterKnife.bind(this);

        initEditText();
        initPresenter();
        attachView();
    }

    private void initEditText() {
        statusText.setEnabled(false);
        statusText.setOnEditorActionListener(this);
        aboutYourselfText.setEnabled(false);
        aboutYourselfText.setOnEditorActionListener(this);
        aboutYourselfText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        aboutYourselfText.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    @OnClick(R.id.back_btn)
    void backClick() {
        onBackPressed();
    }

    @OnClick(R.id.edit_profile_btn)
    void editProfileClick() {
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.edit_status_text)
    void editStatusTextClick() {
        statusText.setEnabled(true);
        statusText.setSelection(statusText.getText().length());
        showKeyboard(statusText);
    }

    @OnClick(R.id.edit_about_yourself_text)
    void editAboutYourselfTextClick() {
        aboutYourselfText.setEnabled(true);
        aboutYourselfText.setSelection(aboutYourselfText.getText().length());
        showKeyboard(aboutYourselfText);
    }

    @OnClick(R.id.exit_from_account_btn)
    public void exitFromAccountBtnClick() {
        Intent intent = new Intent(this, EnterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void showKeyboard(EditText editText) {
        final InputMethodManager inputMethodManager = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    private void hideKeyBoard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void initPresenter(){
        presenter = new MyProfilePresenter();
        presenter.attachView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {

            switch (textView.getId()) {
                case R.id.status_text:
                    presenter.saveStatusClick();
                    hideKeyBoard(statusText);
                    break;
                case R.id.about_yourself_text:
                    presenter.saveAboutYourselfClick();
                    hideKeyBoard(aboutYourselfText);
                    break;
            }

            return true;
        } else
            return false;
    }

    @Override
    public String getStatusText() {
        return statusText.getText().toString();
    }

    @Override
    public String getAboutYourselfText() {
        return aboutYourselfText.getText().toString();
    }

    @Override
    public void setStatusToUI(String text) {
        statusText.setText(text);
        statusText.setEnabled(false);
        Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setAboutYourselfToUI(String text) {
        aboutYourselfText.setText(text);
        aboutYourselfText.setEnabled(false);
        Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT).show();
    }
}
