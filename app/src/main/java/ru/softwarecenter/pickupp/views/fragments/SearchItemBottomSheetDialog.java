package ru.softwarecenter.pickupp.views.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.SearchRideUserAdapter;
import ru.softwarecenter.pickupp.interfaces.SearchItemBottomSheetInterface;
import ru.softwarecenter.pickupp.models.User;
import ru.softwarecenter.pickupp.utils.LockBottomSheetBehavior;

@SuppressLint("ValidFragment")
public class SearchItemBottomSheetDialog extends BottomSheetDialogFragment {
    private Context context;
    private LockBottomSheetBehavior behavior;
    private View contentView;
    private int oldHeight = 0;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }



        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    private SearchItemBottomSheetInterface sheetInterface = null;

    public void setSheetInterface(SearchItemBottomSheetInterface searchItemBottomSheetInterface){
        this.sheetInterface = searchItemBottomSheetInterface;
    }

    public SearchItemBottomSheetDialog(Context context) {
        this.context = context;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        contentView = View.inflate(getContext(), R.layout.card_sheet_current_ride_item, null);
        dialog.setContentView(contentView);
        try {
            FrameLayout bottomSheet = dialog.getWindow().findViewById(android.support.design.R.id.design_bottom_sheet);
            bottomSheet.setBackgroundResource(android.R.color.transparent);
        }catch (NullPointerException e){
            e.printStackTrace();
            dialog.dismiss();
            return;
        }
        initView();
    }

    private void initView(){
        SearchRideUserAdapter adapter = new SearchRideUserAdapter(getContext());
        List<User> users = new ArrayList<>();
        for(int i = 0; i < 6; i++){
            users.add(new User());
        }
        ExpandableListView list = contentView.findViewById(R.id.userList);
        list.setAdapter(adapter);
        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPos, long l) {
                expandableListView.setSelection(0);
                contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        contentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int height = contentView.getMeasuredHeight();
                        behavior.setPeekHeight(height);
                    }
                });
                return false;
            }
        });
        adapter.addUsers(users);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        LockBottomSheetBehavior lock = new LockBottomSheetBehavior();
        params.setBehavior(lock);
        behavior = (LockBottomSheetBehavior) params.getBehavior();

        if( behavior != null) {
            behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback);
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    contentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = contentView.getMeasuredHeight();
                    oldHeight = height;
                    behavior.setPeekHeight(height);
                }
            });
        }

        contentView.findViewById(R.id.lets_ride_btn).setOnClickListener(view -> {
            if(sheetInterface != null){
                sheetInterface.onLetsRideClick();
            }
        });

        contentView.findViewById(R.id.close_sheet).setOnClickListener(view -> {
            if(sheetInterface != null){
                sheetInterface.onCloseClick();
            }
        });
    }
}
