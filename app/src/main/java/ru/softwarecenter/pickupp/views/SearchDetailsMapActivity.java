package ru.softwarecenter.pickupp.views;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.callback.DragStateListener;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.BottomSheetInterface;
import ru.softwarecenter.pickupp.interfaces.BottomToSheetInterface;
import ru.softwarecenter.pickupp.interfaces.SearchDetailsMapInterface;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.presenters.SearchDetailsMapPresenter;
import ru.softwarecenter.pickupp.presenters.SearchRidePresenter;
import ru.softwarecenter.pickupp.utils.Constants;
import ru.softwarecenter.pickupp.views.fragments.MapBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.MapToBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.SearchMapBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.SearchMapToBottomSheetDialogFragment;
import ru.softwarecenter.pickupp.views.fragments.TimeDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static ru.softwarecenter.pickupp.presenters.SearchDetailsMapPresenter.PERMISSION_LOCATION_REQUEST_CODE;

public class SearchDetailsMapActivity extends AppCompatActivity implements SearchDetailsMapInterface.View {

    private SearchDetailsMapPresenter presenter;

    @BindView(R.id.adress)
    TextView adress;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.changeAdress)
    TextView changeAdress;

    @BindView(R.id.buble)
    ImageView buble;

    @BindView(R.id.glass_icon)
    ImageView glassIcon;

    @BindView(R.id.bottom_button_text)
    TextView bottomTxt;

    @BindView(R.id.bottomBackBtn)
    CardView bottomBackBtn;

    @BindView(R.id.rootView)
    ConstraintLayout rootView;

    SearchMapBottomSheetDialogFragment sheetFromFragment;
    SearchMapToBottomSheetDialogFragment sheetToFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_details_map_layout);

        ButterKnife.bind(this);
        initPresenter();
        attachView();
    }

    private void initPresenter(){
        presenter = new SearchDetailsMapPresenter();
        presenter.attachView(this);
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, presenter)
                .addConnectionCallbacks(presenter)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
        presenter.setGoogleApiClient(mGoogleApiClient);

        Intent intent = getIntent();
        presenter.setCurrentMapState(intent.getStringExtra("statePoint"));

        Ride ride = getIntent().getParcelableExtra(Ride.class.getCanonicalName());
        presenter.setRide(ride);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @Override
    public void updateTitle(String title) {
        adress.setText(title);
        if(!title.equals("")) {
            YoYo.with(Techniques.FadeIn).duration(100).playOn(adress);
            YoYo.with(Techniques.FadeIn).duration(100).playOn(changeAdress);
        }
    }

    @Override
    public void hideUI() {
        YoYo.with(Techniques.FadeOut).duration(100).playOn(adress);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(title);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(changeAdress);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(bottomBackBtn);
    }

    @Override
    public void showUI() {
        YoYo.with(Techniques.FadeIn).duration(100).playOn(title);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(bottomBackBtn);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(changeAdress);
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @OnClick(R.id.bottomBackBtn)
    public void bottomBackBtnClick(){
        presenter.bottomBackBtnClick();
    }

    @OnClick(R.id.changeAdress)
    public void changeAdressClick(){
        presenter.openSheet();
    }

    @Override
    public void showPermissionMessage() {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtn, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(SearchDetailsMapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_LOCATION_REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public void openSettingsForGrantPermission(final int REQUEST_CODE) {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtnSettings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(appSettingsIntent, REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public void openFromBottomSheet() {
        SearchMapBottomSheetDialogFragment bottomSheetDialogFragment = new SearchMapBottomSheetDialogFragment( this);
        sheetFromFragment = bottomSheetDialogFragment;

        bottomSheetDialogFragment.setSheetInterface(new BottomSheetInterface() {
            @Override
            public void onCloseClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onItemClick(Street street) {

            }

            @Override
            public void onMapClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }
        });
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void openToBottomSheet() {
        SearchMapToBottomSheetDialogFragment bottomSheetDialogFragment = new SearchMapToBottomSheetDialogFragment(this);
        sheetToFragment = bottomSheetDialogFragment;

        bottomSheetDialogFragment.setSheetInterface(new BottomToSheetInterface() {
            @Override
            public void onCloseClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onItemClick(Street street) {

            }

            @Override
            public void onMapClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }

            @Override
            public void onFromViewClick() {
                bottomSheetDialogFragment.getDialog().dismiss();
            }


        });
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void setViewStateTo() {
        buble.setImageResource(R.drawable.buble_to);
        glassIcon.setImageResource(R.drawable.back_arrow_red);
        title.setText("Куда:");
    }

    @Override
    public void setViewStateFrom() {
        buble.setImageResource(R.drawable.buble_from);
        glassIcon.setImageResource(R.drawable.back_arrow_blue);
        title.setText("Откуда:");

    }

    @Override
    public void backToSearchScreen(Ride ride) {
        Intent intent = new Intent();
        intent.putExtra(Ride.class.getCanonicalName(), ride);

        setResult(0, intent);

        this.finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Ride.class.getCanonicalName(), presenter.getRide());

        setResult(0, intent);

        super.onBackPressed();
    }

    public SearchDetailsMapInterface.Presenter getPresenter(){
        if(presenter != null){
            return presenter;
        }else{
            return null;
        }
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public SupportMapFragment getMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    public void onAdressFromCardClick(View view) {
        Address address = (Address) view.getTag();
        presenter.onAddressFromCardClick(address);

        sheetFromFragment.dismiss();
    }

    public void onAdressToCardClick(View view) {
        Address address = (Address) view.getTag();
        presenter.onAddressToCardClick(address);

        sheetToFragment.dismiss();
    }


}
