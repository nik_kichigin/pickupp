package ru.softwarecenter.pickupp.views.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.utils.Constants;

@SuppressLint("ValidFragment")
public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    TextView dateText;
    public DateDialog (View view) {
        dateText = (TextView)view;
    }

    public Dialog onCreateDialog(Bundle savedInstatceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), R.style.DatePickerTheme, this,year,month,day);
    }

    @SuppressLint("SetTextI18n")
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String date = day + "." + (month + 1) + "." + year;

        long longDate = 1;

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date d = f.parse(date);
            longDate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        date = new java.text.SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).format(longDate);

        dateText.setText(" " + date);
    }

}
