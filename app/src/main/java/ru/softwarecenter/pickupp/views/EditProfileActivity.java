package ru.softwarecenter.pickupp.views;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.EditProfileInterface;
import ru.softwarecenter.pickupp.presenters.EditProfilePresenter;
import ru.softwarecenter.pickupp.utils.ImageUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static ru.softwarecenter.pickupp.presenters.EditProfilePresenter.PERMISSION_STORAGE_REQUEST_CODE;

public class EditProfileActivity extends AppCompatActivity implements EditProfileInterface.View{

    public static final int CAMERA_REQUEST_CODE = 45256;
    public static final int GALLERY_REQUEST_CODE = 53457;

    @BindView(R.id.personName)
    TextView personName;

    @BindView(R.id.personAge)
    TextView personAge;

    @BindView(R.id.sexMan)
    TextView sexMan;

    @BindView(R.id.sexWoman)
    TextView sexWoman;

    @BindView(R.id.rootView)
    CoordinatorLayout rootView;

    @BindView(R.id.avatarPlace)
    ImageView avatarImage;

    @BindView(R.id.avatar)
    CardView avatar;

    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;

    private EditProfilePresenter presenter;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);

        ButterKnife.bind(this);
        presenter = new EditProfilePresenter();
        initToolbar();
    }

    @OnClick(R.id.changeNumber)
    void changeNumberClick() {
        Intent intent = new Intent(this, ChangeNumberActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @OnClick(R.id.sexMan)
    void sexManClick(){
        presenter.setSex(1);
    }

    @OnClick(R.id.sexWoman)
    void sexWomanClick(){
        presenter.setSex(0);
    }

    @OnTextChanged(R.id.personName)
    void OnTextChanged(CharSequence s, int start, int count, int after){
        if(s != null){
            if(s.length() > 0){
                presenter.setName(s.toString());
            }else{
                presenter.clearName();
            }
        }
    }

    @OnTextChanged(R.id.personAge)
    void personAgeTextChanged(CharSequence s, int start, int count, int after){
        if(s != null){
            if(s.length() > 0){
                presenter.setAge(Integer.parseInt(s.toString()));
            }else{
                presenter.clearAge();
            }
        }
    }

    @OnClick(R.id.avatar)
    void addAvatar(){
        showDialog(presenter.isAvatarAdded());
    }

    @OnClick(R.id.floatingActionButton)
    void changeAvatar(){
        showDialog(presenter.isAvatarAdded());
    }

    @Override
    public boolean isNameAdded() {
        return (personName.length() > 0);
    }

    @Override
    public boolean isAgeAdded() {
        return (personAge.length() > 0);
    }

    private void showDialog(boolean hasAvatar){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Откуда взять фотографию?");
        builder.setPositiveButton("Сделать фото", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(presenter.checkPermission()) {
                    presenter.setAvatarFromCamera(CAMERA_REQUEST_CODE);
                }else{
                    dialogInterface.dismiss();
                }
            }
        });
        builder.setNegativeButton("Выбрать из галереи", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(presenter.checkPermission()) {
                    presenter.setAvatarFromGallery(GALLERY_REQUEST_CODE);
                }else{
                    dialogInterface.dismiss();
                }
            }
        });
        if(hasAvatar){
            builder.setNeutralButton("Удалить", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    presenter.removeAvatar();
                }
            });
        }else{
            builder.setNeutralButton("Отмена", null);
        }
        builder.setCancelable(true);
        builder.create().show();
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @OnClick(R.id.enter_btn)
    @Override
    public void openEditProfileScreen() {
        presenter.enterBtnClick();
    }

    @Override
    public void setManSexSelected() {
        sexMan.setTextColor(Color.WHITE);
        sexMan.setBackgroundResource(R.color.colorAccent);
    }

    @Override
    public void setWomanSexSelected() {
        sexWoman.setBackgroundResource(R.color.colorAccent);
        sexWoman.setTextColor(Color.WHITE);
    }

    @Override
    public void setSexDeselected() {
        sexMan.setTextColor(Color.DKGRAY);
        sexMan.setBackgroundResource(R.color.colorWhite);
        sexWoman.setBackgroundResource(R.color.colorWhite);
        sexWoman.setTextColor(Color.DKGRAY);
    }

    @Override
    public void setAvatarImage(Bitmap bitmap) {
        avatarImage.setImageBitmap(bitmap);
        floatingActionButton.setVisibility(View.VISIBLE);
        avatar.setEnabled(false);
    }

    @Override
    public void removeAvatar() {
        avatarImage.setImageBitmap(null);
        floatingActionButton.setVisibility(View.GONE);
        avatar.setEnabled(true);
    }

    @Override
    public void onBackScreen() {
        onBackPressed();
    }

    @Override
    public void showPermissionMessage() {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtn, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(EditProfileActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_STORAGE_REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public void openSettingsForGrantPermission(final int REQUEST_CODE) {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtnSettings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(appSettingsIntent, REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            return;
        }
        if(requestCode == PERMISSION_STORAGE_REQUEST_CODE){
            if ((grantResults.length > 0) && (grantResults[0] + grantResults[1]) == PackageManager.PERMISSION_GRANTED){
                showDialog(presenter.isAvatarAdded());
            }else{
                openSettingsForGrantPermission(PERMISSION_STORAGE_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            return;
        }
        if(requestCode == CAMERA_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                presenter.generateBitmap(null);
            }
        }else if(requestCode == GALLERY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                if(data.getData() != null){
                    presenter.generateBitmap(ImageUtils.getRealPathFromURI(this, data.getData()));
                }else{
                    showMessage(R.string.exeptionError);
                }
            }
        }
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.edit_profile);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }
}
