package ru.softwarecenter.pickupp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.CategorySupportAdapter;
import ru.softwarecenter.pickupp.interfaces.TechnicalSupportInterface;
import ru.softwarecenter.pickupp.presenters.TechnicalSupportPresenter;


public class TechnicalSupportActivity extends AppCompatActivity implements TechnicalSupportInterface.View{

    TechnicalSupportPresenter presenter;

    @BindView(R.id.email_text_card)
    CardView emailTextCard;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @BindView(R.id.edit_message_edit_text)
    EditText editMessageText;

    @BindView(R.id.edit_email_edit_text)
    EditText editEmailText;

    @BindView(R.id.send_message_btn)
    CardView sendMessageBtn;

    @BindView(R.id.id_category_arrow)
    ImageView categoryArrow;

    @BindView(R.id.id_path_communicate_arrow)
    ImageView pathCommunicateArrow;

    @BindView(R.id.idSelectCathegoryListView)
    ExpandableListView listViewCathegory;

    @BindView(R.id.idPathComunicateListView)
    ExpandableListView listViewPathComunicate;

    @BindView(R.id.root_layout)
    CoordinatorLayout rootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.technical_support_activity);

        ButterKnife.bind(this);

        initToolbar();
        initPresenter();
        attachView();
    }

    private void initPresenter(){
        presenter = new TechnicalSupportPresenter();
        presenter.attachView(this);
        presenter.initCategoryArrays();
        presenter.initPathComunicateArrays();
        presenter.getAdapterCathegory(this);
        presenter.getAdapterPathComunicate(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @OnClick(R.id.send_message_btn)
    void sendMessageBtnClick() {
        presenter.onSendMessageBtnClick();
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @Override
    public String getEmailText() {
        return editEmailText.getText().toString();
    }

    @Override
    public String getMessageText() {
        return editMessageText.getText().toString();
    }

    @Override
    public void sendMessage() {
        Toast.makeText(getApplicationContext(), "Письмо отправлено", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.tech_support);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }

    @Override
    public void setCathegoryListView(CategorySupportAdapter adapter) {
        listViewCathegory.setAdapter(adapter);

        listViewCathegory.setOnGroupExpandListener(groupPosition -> {
            categoryArrow.setImageResource(R.drawable.arrow_light_gray_up);
            listViewPathComunicate.collapseGroup(0);

        });

        listViewCathegory.setOnGroupCollapseListener(groupPosition -> {
            categoryArrow.setImageResource(R.drawable.arrow_light_gray_down);
        });

        listViewCathegory.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            parent.collapseGroup(groupPosition);
            presenter.onCategoryChildItemClick(groupPosition, childPosition);

            return false;
        });
    }

    @Override
    public void setPathComunicateListView(CategorySupportAdapter adapter) {
        listViewPathComunicate.setAdapter(adapter);

        listViewPathComunicate.setOnGroupExpandListener(groupPosition -> {
            pathCommunicateArrow.setImageResource(R.drawable.arrow_light_gray_up);
            listViewCathegory.collapseGroup(0);
        });

        listViewPathComunicate.setOnGroupCollapseListener(groupPosition -> {
            pathCommunicateArrow.setImageResource(R.drawable.arrow_light_gray_down);
        });

        listViewPathComunicate.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            parent.collapseGroup(groupPosition);
            presenter.onPathComunicateChildItemClick(groupPosition, childPosition);

            if (childPosition == 1) {
                emailTextCard.setVisibility(View.VISIBLE);
                presenter.setEmailFieldOpenState(true);
            } else {
                emailTextCard.setVisibility(View.GONE);
                presenter.setEmailFieldOpenState(false);
            }

            return false;
        });
    }

}
