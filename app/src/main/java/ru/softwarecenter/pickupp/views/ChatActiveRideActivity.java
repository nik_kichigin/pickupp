package ru.softwarecenter.pickupp.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMessagesAdapter;
import ru.softwarecenter.pickupp.interfaces.ChatActiveRideInterface;
import ru.softwarecenter.pickupp.presenters.ChatActiveRidePresenter;

public class ChatActiveRideActivity extends AppCompatActivity implements ChatActiveRideInterface.View{

    ChatActiveRidePresenter presenter;

    @BindView(R.id.edit_message_text)
    EditText editMessageText;

    @BindView(R.id.messagesRecyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);

        ButterKnife.bind(this);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        initPresenter();
        attachView();
    }

    private void initPresenter(){
        presenter = new ChatActiveRidePresenter();
        presenter.attachView(this);
        presenter.initRecyclerView(this, recyclerView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @OnClick(R.id.profile_layout)
    void avatarImageClick() {
        Intent intent = new Intent(this, AnotherProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ride_details_btn)
    void rideDetailsBtnClick() {
        Intent intent = new Intent(this, ActiveRideActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.back_btn)
    void backButtonClick() {
        onBackPressed();
    }

    @Override
    public void setAdapterToRV(RVMessagesAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.send_message)
    void sendMessageBtnClick() {
        presenter.sendMessage(editMessageText.getText().toString());
        editMessageText.setText("");
    }
}
