package ru.softwarecenter.pickupp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.RatingRideInterface;
import ru.softwarecenter.pickupp.presenters.RatingRidePresenter;

public class RatingRideActivity extends AppCompatActivity implements RatingRideInterface.View{

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @BindView(R.id.scaleRatingBar)
    ScaleRatingBar ratingBar;

    RatingRidePresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_ride_layout);

        ButterKnife.bind(this);

        ratingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(BaseRatingBar baseRatingBar, float v) {
                presenter.setRatingValue(v);
            }
        });

        initToolbar();
        initPresenter();
        attachView();
    }

    @OnClick(R.id.btn_done)
    void btnDoneClick(){
        onBackPressed();
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.end_ride);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }

    private void initPresenter() {
        presenter = new RatingRidePresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }


}
