package ru.softwarecenter.pickupp.views;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.willy.ratingbar.ScaleRatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.expandable_list_adapter.TravelDetailsAdapter;
import ru.softwarecenter.pickupp.interfaces.PreviousRideInterface;
import ru.softwarecenter.pickupp.presenters.PreviousRidePresenter;

public class PreviousRideActivity extends AppCompatActivity implements PreviousRideInterface.View{

    PreviousRidePresenter presenter;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    /*@BindView(R.id.id_about_traveler_arrow)
    ImageView aboutTravelerArrow;*/

    @BindView(R.id.id_travel_details_arrow)
    ImageView travelDetailsArrow;

    @BindView(R.id.idRideDetailListView)
    ExpandableListView listViewTravelDetails;

    /*@BindView(R.id.idAboutFellowTravelerListView)
    ExpandableListView listViewAboutTraveler;*/



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.previous_ride_layout);

        ButterKnife.bind(this);

        initToolbar();
        initPresenter();
        attachView();
    }

    private void initPresenter(){
        presenter = new PreviousRidePresenter();
        presenter.attachView(this);
        //presenter.initAboutTravelerArrays();
        presenter.initTravelDetailsArrays();
        //presenter.getAdapterAboutTraveler(this);
        presenter.getAdapterTravelDetails(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.previous_ride);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }

    @OnClick(R.id.from_location_btn)
    void fromLocationBtnClick() {
        Intent intent = new Intent(this, DetailsMapActivity.class);
        intent.putExtra("marker_state", "from");
        startActivity(intent);
    }

    @OnClick(R.id.to_location_btn)
    void toLocationBtnClick() {
        Intent intent = new Intent(this, DetailsMapActivity.class);
        intent.putExtra("marker_state", "to");
        startActivity(intent);
    }

    @OnClick(R.id.open_profile_btn)
    void openProfileBtnClick() {
        Intent intent = new Intent(this, AnotherProfileActivity.class);
        startActivity(intent);
    }


    @Override
    public void setTravelDetailsListView(TravelDetailsAdapter adapter) {
        listViewTravelDetails.setAdapter(adapter);

        listViewTravelDetails.setOnGroupExpandListener(groupPosition -> {
            travelDetailsArrow.setImageResource(R.drawable.arrow_light_gray_up);
            //listViewAboutTraveler.collapseGroup(0);
        });

        listViewTravelDetails.setOnGroupCollapseListener(groupPosition -> {
            travelDetailsArrow.setImageResource(R.drawable.arrow_light_gray_down);
        });

        listViewTravelDetails.setOnChildClickListener((expandableListView, view, i, i1, l) -> {
            Intent intent = new Intent(PreviousRideActivity.this, RatingRideActivity.class);
            startActivity(intent);

            return false;
        });
    }
}
