package ru.softwarecenter.pickupp.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.DetailsMapInterface;
import ru.softwarecenter.pickupp.presenters.DetailsMapPresenter;

import static ru.softwarecenter.pickupp.presenters.MapPresenter.PERMISSION_LOCATION_REQUEST_CODE;

public class DetailsMapActivity extends AppCompatActivity implements DetailsMapInterface.View{

    DetailsMapPresenter presenter;

    private String marker_state;

    @BindView(R.id.adress)
    TextView adress;

    @BindView(R.id.menu)
    ImageButton menu;

    @BindView(R.id.locationBtn)
    ImageView locationBtn;

    @BindView(R.id.rootView)
    CoordinatorLayout rootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_map_layout);

        Intent intent = getIntent();
        marker_state = intent.getStringExtra("marker_state");

        ButterKnife.bind(this);
        initPresenter();
        attachView();
    }

    private void initPresenter(){
        presenter = new DetailsMapPresenter();
        presenter.attachView(this);
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, presenter)
                .addConnectionCallbacks(presenter)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
        presenter.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @OnClick(R.id.locationBtn)
    public void locationClick(){
        presenter.locationClick();
    }

    @Override
    public void updateTitle(String title) {
        adress.setText(title);
        if(!title.equals("")) {
            YoYo.with(Techniques.FadeIn).duration(100).playOn(adress);
        }
    }

    @Override
    public void hideUI() {
        YoYo.with(Techniques.FadeOut).duration(100).playOn(adress);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(menu);
        YoYo.with(Techniques.FadeOut).duration(100).playOn(locationBtn);

    }

    @Override
    public void showUI() {
        YoYo.with(Techniques.FadeIn).duration(100).playOn(menu);
        YoYo.with(Techniques.FadeIn).duration(100).playOn(locationBtn);
    }

    @Override
    public SupportMapFragment getMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            return;
        }
        if(requestCode == PERMISSION_LOCATION_REQUEST_CODE){
            if ((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                presenter.updateLocation();
            }else{
                openSettingsForGrantPermission(PERMISSION_LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void openSettingsForGrantPermission(final int REQUEST_CODE) {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtnSettings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(appSettingsIntent, REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

    @Override
    public String getMarkerState() {
        return marker_state;
    }

    @Override
    public void showPermissionMessage() {
        Snackbar snackbar = Snackbar.make(rootView, R.string.getPermissionMessage, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.getPermissionMessageBtn, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(DetailsMapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_LOCATION_REQUEST_CODE);
            }
        });
        snackbar.setDuration(3000);
        snackbar.show();
    }

}
