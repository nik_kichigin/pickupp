package ru.softwarecenter.pickupp.views.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;

@SuppressLint("ValidFragment")
public class TimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    TextView timeText;
    public TimeDialog (View view) {
        timeText = (TextView)view;
    }

    @SuppressLint("SimpleDateFormat")
    public Dialog onCreateDialog(Bundle savedInstatceState) {
        //final Calendar c = Calendar.getInstance();
        //int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        //int minute = c.get(Calendar.MINUTE);

        String date = timeText.getText().toString();

        long longDate = 1;

        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
        try {
            Date d = f.parse(date);
            longDate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        int hourOfDay = Integer.parseInt(new SimpleDateFormat("HH", Locale.ENGLISH).format(longDate));
        int minute = Integer.parseInt(new SimpleDateFormat("mm", Locale.ENGLISH).format(longDate));

        return new TimePickerDialog(getActivity(), R.style.TimePickerTheme,  this, hourOfDay, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String time = hourOfDay + ":" + minute;

        long longDate = 1;

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
        try {
            Date d = f.parse(time);
            longDate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        time = new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(longDate);

        timeText.setText(time);
    }
}
