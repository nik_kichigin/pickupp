package ru.softwarecenter.pickupp.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVHistoryRequestsAdapter;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMyCurrentRequestsAdapter;
import ru.softwarecenter.pickupp.data_store.database.HistoryRequestsHandler;
import ru.softwarecenter.pickupp.interfaces.HistoryRequestsInterface;
import ru.softwarecenter.pickupp.interfaces.OnLoadMoreListener;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.presenters.HistoryRequestsPresenter;
import ru.softwarecenter.pickupp.utils.Constants;

public class HistoryRequestsActivity extends AppCompatActivity implements HistoryRequestsInterface.View{

    HistoryRequestsPresenter presenter;

    @BindView(R.id.idHistoryRequestsRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_requests_layout);

        ButterKnife.bind(this);

        initToolbar();
        initPresenter();
        attachView();
    }

    public void onCardHistoryRideClick(View view) {
        Intent intent = new Intent(this, PreviousRideActivity.class);
        startActivity(intent);
    }

    private void initPresenter(){
        presenter = new HistoryRequestsPresenter();
        presenter.attachView(this);
        presenter.initRecyclerView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.history);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }

    @Override
    public RecyclerView getRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        return recyclerView;
    }

    @Override
    public void setAdapterToRV(RVHistoryRequestsAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }
}
