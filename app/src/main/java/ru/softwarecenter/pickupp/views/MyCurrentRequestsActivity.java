package ru.softwarecenter.pickupp.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVMyCurrentRequestsAdapter;
import ru.softwarecenter.pickupp.interfaces.MyCurrentRequestsInterface;
import ru.softwarecenter.pickupp.models.Ride;
import ru.softwarecenter.pickupp.models.Street;
import ru.softwarecenter.pickupp.presenters.MyCurrentRequestsPresenter;
import ru.softwarecenter.pickupp.utils.Constants;

public class MyCurrentRequestsActivity extends AppCompatActivity implements MyCurrentRequestsInterface.View{

    MyCurrentRequestsPresenter presenter;

    @BindView(R.id.idMyCurrentRequestsRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_current_requests_layout);

        ButterKnife.bind(this);

        initToolbar();
        initPresenter();
        attachView();
        setTestInfoToRV();
    }

    private void initPresenter(){
        presenter = new MyCurrentRequestsPresenter();
        presenter.attachView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    private void setTestInfoToRV() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        RVMyCurrentRequestsAdapter adapter = presenter.getMyCurrentRequestsAdapter();
        recyclerView.setAdapter(adapter);
    }


    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.my_requests);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }

    public void onCardCurrentRideClick(View view) {
        Intent intent = new Intent(this, ChatActiveRideActivity.class);
        startActivity(intent);
    }
}
