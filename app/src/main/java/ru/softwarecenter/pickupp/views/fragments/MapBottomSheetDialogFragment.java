package ru.softwarecenter.pickupp.views.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;

import java.util.List;


import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVAddressesFromAdapter;
import ru.softwarecenter.pickupp.data_store.database.AddressesFromMapHandler;
import ru.softwarecenter.pickupp.interfaces.BottomSheetInterface;
import ru.softwarecenter.pickupp.presenters.MapPresenter;
import ru.softwarecenter.pickupp.views.MapActivity;

@SuppressLint("ValidFragment")
public class MapBottomSheetDialogFragment extends BottomSheetDialogFragment {

    MapPresenter presenter;
    List<Address> adresses;
    Context context;

    List<Address> adressesSearch;
    RVAddressesFromAdapter adapter;


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                presenter = null;
                adresses.clear();
                context = null;
                if (adressesSearch != null) {
                    adressesSearch.clear();
                }

                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };
    private BottomSheetInterface sheetInterface = null;


    public MapBottomSheetDialogFragment(Context context) {
        this.context = context;
    }

    public void setSheetInterface(BottomSheetInterface bottomSheetInterface){
        this.sheetInterface = bottomSheetInterface;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View contentView = View.inflate(getContext(), R.layout.from_bottom_sheet_layout, null);
        dialog.setContentView(contentView);
        FrameLayout bottomSheet = dialog.getWindow().findViewById(android.support.design.R.id.design_bottom_sheet);
        bottomSheet.setBackgroundResource(R.drawable.card_white_rounded_background);
        initView(contentView);

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        presenter = null;
        adresses.clear();
        context = null;
        if (adressesSearch != null) {
            adressesSearch.clear();
        }
    }

    private void initView(View contentView) {
        AddressesFromMapHandler db = new AddressesFromMapHandler(context);
        adresses = db.getAllAddresses();

        //db.deleteAllAddresses();

        //presenter = new MapPresenter();
        presenter = (MapPresenter) ((MapActivity) getContext()).getPresenter();
        if(presenter != null) {
            RecyclerView recyclerView = contentView.findViewById(R.id.rvAdreses);
            EditText editAdressFrom = contentView.findViewById(R.id.editAdressFrom);

            LinearLayoutManager llm = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(llm);
            adapter = new RVAddressesFromAdapter(adresses);
            recyclerView.setAdapter(adapter);

            editAdressFrom.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    String adress = editAdressFrom.getText().toString();

                    final Handler handler = new Handler();
                    final Runnable uiRunnable = () -> {

                        adapter.notifyDataSetChanged();
                    };

                    Thread thread = new Thread(() -> {

                        adresses.clear();

                        if (adress.length() > 0) {
                            adresses.addAll(presenter.getAdresses(adress, context));
                        } else {
                            AddressesFromMapHandler db = new AddressesFromMapHandler(context);
                            adresses.addAll(db.getAllAddresses());
                        }

                        handler.post(uiRunnable);
                    });

                    thread.start();





                    Thread thread1 = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            adresses.clear();

                            if (adress.length() > 0) {
                                adresses.addAll(presenter.getAdresses(adress, context));
                            } else {
                                AddressesFromMapHandler db = new AddressesFromMapHandler(context);
                                adresses.addAll(db.getAllAddresses());
                            }



                        }
                    });


                    thread1.start();










                }
            });

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
                contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        contentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int height = contentView.getMeasuredHeight();
                        ((BottomSheetBehavior) behavior).setPeekHeight(height);
                    }
                });
            }
            contentView.findViewById(R.id.closeBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sheetInterface != null) {
                        sheetInterface.onCloseClick();
                    }
                }
            });
            contentView.findViewById(R.id.closeBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sheetInterface != null) {
                        sheetInterface.onCloseClick();
                    }
                }
            });
            contentView.findViewById(R.id.mapBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sheetInterface != null) {
                        sheetInterface.onMapClick();
                    }
                }
            });

        }
    }


}
