package ru.softwarecenter.pickupp.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vicmikhailau.maskededittext.MaskedEditText;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.ChangeNumberInterface;
import ru.softwarecenter.pickupp.interfaces.EnterInterface;
import ru.softwarecenter.pickupp.presenters.ChangeNumberPresenter;
import ru.softwarecenter.pickupp.presenters.EnterPresenter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangeNumberActivity extends AppCompatActivity implements ChangeNumberInterface.View{

    @BindView(R.id.phone_input)
    MaskedEditText phone;

    @BindView(R.id.code_input)
    EditText sms_code;

    @BindView(R.id.phone_timer)
    TextView timer;

    @BindView(R.id.code)
    View codeIcon;

    @BindView(R.id.phone)
    View phoneIcon;

    @BindView(R.id.rootView)
    CoordinatorLayout rootView;

    @BindView(R.id.phone_send)
    TextView phoneBtn;

    @BindView(R.id.phone_send_btn)
    LinearLayout phoneBtnLayout;

    private int time;

    private ChangeNumberPresenter presenter;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_number_layout);

        ButterKnife.bind(this);
        codeIcon.setEnabled(false);
        phoneIcon.setEnabled(false);
        phone.setCursorVisible(false);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phone.getUnMaskedText().length() < 1){
                    phone.setText("+7 (");
                    phone.setSelection(phone.getText().toString().length());
                }
                phoneIcon.setEnabled(true);
                phone.setCursorVisible(true);
            }
        });
        sms_code.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    codeIcon.setEnabled(true);
                }else{
                    if(sms_code.getText().length() < 1){
                        codeIcon.setEnabled(false);
                    }
                }
            }
        });

        initToolbar();
        initPresenter();
        attachView();
    }

    private void initPresenter() {
        presenter = new ChangeNumberPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @OnClick(R.id.enter_btn)
    void click(){
        presenter.codeFiled(sms_code.getText().toString(), phone.getUnMaskedText());
    }

    @OnClick(R.id.phone_send_btn)
    void onSendPhoneBtnClick(){
        presenter.phoneFiled(phone.getUnMaskedText());
    }

    @Override
    public void updateButtonSendPhone() {
        phoneBtn.setText("Отправить повторно");
        phoneBtn.setEnabled(false);
        phoneBtnLayout.setGravity(Gravity.START);
        timer.setVisibility(View.VISIBLE);
        Timer timer1 = new Timer();
        time = 60;
        timer1.schedule(new TimerTask() {
            @Override
            public void run() {
                if(time > 0){
                    time--;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(time < 10){
                                timer.setText("0:0" + time);
                            }else{
                                timer.setText("0:" + time);
                            }
                        }
                    });
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timer.setVisibility(View.GONE);
                            phoneBtn.setEnabled(true);
                            ((LinearLayout) findViewById(R.id.phone_send_btn)).setGravity(Gravity.CENTER);
                        }
                    });
                    this.cancel();
                }
            }
        }, 0 ,1000);
    }

    @Override
    public void setFocusOnCodeField() {
        sms_code.requestFocus();
        codeIcon.setEnabled(true);
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @Override
    public void openMapScreen() {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.change_number);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }
}
