package ru.softwarecenter.pickupp.views.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.adapters.recycle_view_adapter.RVAddressesToAdapter;
import ru.softwarecenter.pickupp.data_store.database.AddressesToMapHandler;
import ru.softwarecenter.pickupp.interfaces.BottomToSheetInterface;
import ru.softwarecenter.pickupp.presenters.MapPresenter;
import ru.softwarecenter.pickupp.views.MapActivity;

@SuppressLint("ValidFragment")
public class MapToBottomSheetDialogFragment extends BottomSheetDialogFragment {

    MapPresenter presenter;
    List<Address> adresses;
    Context context;

    List<Address> adressesSearch;
    RVAddressesToAdapter adapter;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }



        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };
    private BottomToSheetInterface sheetInterface = null;

    public MapToBottomSheetDialogFragment(Context context) {
        this.context = context;
    }

    public void setSheetInterface(BottomToSheetInterface bottomSheetInterface){
        this.sheetInterface = bottomSheetInterface;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View contentView = View.inflate(getContext(), R.layout.to_bottom_sheet_layout, null);
        dialog.setContentView(contentView);
        FrameLayout bottomSheet = dialog.getWindow().findViewById(android.support.design.R.id.design_bottom_sheet);
        bottomSheet.setBackgroundResource(R.drawable.card_white_rounded_background);
        initView(contentView);

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        presenter = null;
        adresses.clear();
        context = null;
        if (adressesSearch != null) {
            adressesSearch.clear();
        }
    }

    private void initView(View contentView) {
        AddressesToMapHandler db = new AddressesToMapHandler(context);
        adresses = db.getAllAddresses();

        //presenter = new MapPresenter();
        presenter = (MapPresenter) ((MapActivity) getContext()).getPresenter();
        TextView fromText = contentView.findViewById(R.id.from_text);
        fromText.setText(presenter.getFromText());

        //db.deleteAllAddresses();

        if(presenter != null) {
            RecyclerView recyclerView = contentView.findViewById(R.id.rvAdreses);
            EditText editAdressFrom = contentView.findViewById(R.id.editAdressTo);

            LinearLayoutManager llm = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(llm);
            adapter = new RVAddressesToAdapter(adresses);
            recyclerView.setAdapter(adapter);

            editAdressFrom.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    String adress = editAdressFrom.getText().toString();

                    final Handler handler = new Handler();
                    final Runnable uiRunnable = () ->
                            adapter.notifyDataSetChanged();

                    Thread thread = new Thread(() -> {

                        adresses.clear();

                        if (adress.length() > 0) {
                            adresses.addAll(presenter.getAdresses(adress, context));
                        } else {
                            AddressesToMapHandler db = new AddressesToMapHandler(context);
                            adresses.addAll(db.getAllAddresses());
                        }

                        handler.post(uiRunnable);
                    });

                    thread.start();
                }
            });

        }


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    contentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = contentView.getMeasuredHeight();
                    ((BottomSheetBehavior) behavior).setPeekHeight(height);
                }
            });
        }
        contentView.findViewById(R.id.closeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sheetInterface != null){
                    sheetInterface.onCloseClick();
                }
            }
        });
        contentView.findViewById(R.id.mapBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sheetInterface != null){
                    sheetInterface.onMapClick();
                }
            }
        });
        contentView.findViewById(R.id.fromView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sheetInterface != null){
                    sheetInterface.onFromViewClick();
                }
            }
        });


    }
}
