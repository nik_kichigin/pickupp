package ru.softwarecenter.pickupp.adapters.recycle_view_adapter;

import android.annotation.SuppressLint;
import android.location.Address;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.softwarecenter.pickupp.R;

public class RVAddressesToAdapter extends RecyclerView.Adapter<RVAddressesToAdapter.CardViewHolder> {

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView street_house, city_country;

        CardViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.idCardAdressTo);

            street_house = itemView.findViewById(R.id.street_home_txt);
            city_country = itemView.findViewById(R.id.city_country_txt);

        }
    }


    private List<Address> adresses;
    public RVAddressesToAdapter(List<Address> adresses){
        this.adresses = adresses;
    }


    @Override
    public RVAddressesToAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_sheet_adress_to, viewGroup, false);
        RVAddressesToAdapter.CardViewHolder cvh = new RVAddressesToAdapter.CardViewHolder(v);
        return cvh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RVAddressesToAdapter.CardViewHolder cardViewHolder, int i) {
        Address address = adresses.get(i);
        String country = address.getCountryName();
        String city = address.getLocality();
        String street = address.getThoroughfare();
        String house = address.getSubThoroughfare();

        if (street == null)
            street = "";
        if (house == null)
            house = "";
        if (country == null)
            country = "";
        if (city == null)
            city = "";

        cardViewHolder.street_house.setText(street + " " + house);
        cardViewHolder.city_country.setText(city + " " + country);

        cardViewHolder.cv.setTag(address);
    }

    @Override
    public int getItemCount() {
        return adresses.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
