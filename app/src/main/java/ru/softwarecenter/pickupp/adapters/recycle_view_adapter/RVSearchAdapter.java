package ru.softwarecenter.pickupp.adapters.recycle_view_adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.SearchRideItemClick;
import ru.softwarecenter.pickupp.models.Ride;

public class RVSearchAdapter extends RecyclerView.Adapter<RVSearchAdapter.SearchCardViewHolder>{
    private Context context;
    private List<Ride> rides;
    private SearchRideItemClick iClick;

    public static class SearchCardViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.rideRootView)
        CardView view;

        @BindView(R.id.rideTime)
        TextView time;

        @BindView(R.id.rideCost)
        TextView cost;

        @BindView(R.id.rideName)
        TextView name;

        @BindView(R.id.rideSex)
        TextView sex;

        @BindView(R.id.rideRadiusAfter)
        TextView radiusAfter;

        @BindView(R.id.rideRadiusBefore)
        TextView radiusBefore;

        @BindView(R.id.rideStatus)
        CircleImageView rideStatus;

        SearchCardViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Ride ride){
//            time.setText();
//            cost.setText();
//            name.setText();
        }
    }

    public RVSearchAdapter(Context context, RecyclerView recyclerView, SearchRideItemClick iClick) {
        this.context = context;
        rides = new ArrayList<>();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.iClick = iClick;
    }

    @Override
    public SearchCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_small_item, parent, false);
        return new SearchCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchCardViewHolder holder, int position) {
        holder.bind(rides.get(position));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iClick.itemClick(rides.get(position));
            }
        });
    }

    public void addItem(Ride ride){
        rides.add(ride);
        notifyItemInserted(rides.size() - 1);
    }

    public void removeItem(Ride ride){
        int deletedIndex = rides.indexOf(ride);
        if(deletedIndex != -1){
            rides.remove(deletedIndex);
            notifyItemRemoved(deletedIndex);
        }
    }

    public void updateItem(Ride ride){
        int updatedIndex = rides.indexOf(ride);
        if(updatedIndex != -1){
            rides.remove(updatedIndex);
            rides.add(updatedIndex, ride);
            notifyItemChanged(updatedIndex);
        }
    }

    @Override
    public int getItemCount() {
        return rides == null ? 0 : rides.size();
    }
}
