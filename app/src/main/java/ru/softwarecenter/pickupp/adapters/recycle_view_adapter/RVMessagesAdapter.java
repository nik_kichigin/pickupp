package ru.softwarecenter.pickupp.adapters.recycle_view_adapter;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.OnLoadMoreListener;
import ru.softwarecenter.pickupp.models.expandable_list_models.Message;

public class RVMessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private final int VIEW_TYPE_SYSTEM = -1;
    private final int VIEW_TYPE_ITEM_MY = 0;
    private final int VIEW_TYPE_ITEM = 2;
    private final int VIEW_TYPE_LOADING = 1;
    private List<Message> list = new ArrayList<>();
    private OnLoadMoreListener onLoadMore;
    private Context context;
    private boolean isLoading;
    private RecyclerView recyclerView;
    private int visibleThreshold = 10;
    private int firstVisibleItem;

    public RVMessagesAdapter(Context context, RecyclerView recyclerView, List<Message> messages) {
        this.context = context;
        this.list = messages;
        this.recyclerView = recyclerView;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading && (firstVisibleItem - visibleThreshold) <= 0) {
                    if (onLoadMore != null) {
                        onLoadMore.onLoadMore();
                        isLoading = true;
                    }

                }
            }
        });

    }

    public void scrollToLastItem(){
        if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition() == list.size() - 2) {
            recyclerView.scrollToPosition(list.size() - 1);
        }
    }

    public void scrollToLastItemPermoment(){
        recyclerView.scrollToPosition(list.size() - 1);
    }

    public void setLoadMoreListener(OnLoadMoreListener onLoadMore) {
        this.onLoadMore = onLoadMore;
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position) == null) return VIEW_TYPE_LOADING;
        switch (list.get(position).getOwner_user_message()) {
            case "my_message":
                return VIEW_TYPE_ITEM_MY;
                //break;
            case "another_message":
                return VIEW_TYPE_ITEM;
                //break;
            case "system_message":
                return VIEW_TYPE_SYSTEM;
                //break;

        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.another_chat_message, parent, false);
            //View view = LayoutInflater.from(context).inflate(R.layout.another_chat_message, parent, false);
            return new MessageViewHolder(view);
        } else if(viewType == VIEW_TYPE_ITEM_MY) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_chat_message, parent, false);
            //View view = LayoutInflater.from(context).inflate(R.layout.my_chat_message, parent, false);
            return new MessageViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.loading_item_layout, parent, false);
            return new LoadingViewHolder(view);
        } else if (viewType == VIEW_TYPE_SYSTEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.system_chat_message, parent, false);
            return new MessageViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }else{
            final Message message = list.get(position);
            MessageViewHolder viewHolder = (MessageViewHolder) holder;
            viewHolder.message.setText(message.getMessage());
            viewHolder.time.setText(message.getTime());

            View.OnLongClickListener listener = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Действие над сообщением");
                    builder.setPositiveButton("Копировать текст", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("Текст сообщения", message.getMessage());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(context, "Текст скопирован в буфер обмена", Toast.LENGTH_SHORT).show();
                        }
                    });
                    builder.setNeutralButton("Отмена", null);
                    builder.create().show();

                    return false;
                }
            };
            viewHolder.message.setOnLongClickListener(listener);

        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void isLoaded() {
        isLoading = true;
    }



    private static class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView message;
        TextView time;

        MessageViewHolder(View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar1);
        }
    }
}
