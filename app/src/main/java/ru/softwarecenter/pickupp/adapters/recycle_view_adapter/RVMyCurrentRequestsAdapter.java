package ru.softwarecenter.pickupp.adapters.recycle_view_adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.models.Ride;

public class RVMyCurrentRequestsAdapter extends RecyclerView.Adapter<RVMyCurrentRequestsAdapter.CardViewHolder> {

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView dateText, timeText, pointDepartureText, pointDestinationText;

        CardViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.idCardRequest);

            dateText = itemView.findViewById(R.id.idCardDateText);
            timeText = itemView.findViewById(R.id.idCardTimeText);
            pointDepartureText = itemView.findViewById(R.id.idPointOfDepartureText);
            pointDestinationText = itemView.findViewById(R.id.idPointOfDestinationText);
        }
    }


    private List<Ride> allRequestsArray;
    public RVMyCurrentRequestsAdapter(List<Ride> allRequestsArray){
        this.allRequestsArray = allRequestsArray;
    }



    @Override
    public RVMyCurrentRequestsAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_current_request, viewGroup, false);
        RVMyCurrentRequestsAdapter.CardViewHolder cvh = new RVMyCurrentRequestsAdapter.CardViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final RVMyCurrentRequestsAdapter.CardViewHolder cardViewHolder, int i) {
        cardViewHolder.dateText.setText(new java.text.SimpleDateFormat("d MMM").format(allRequestsArray.get(i).getTime()));
        cardViewHolder.timeText.setText(new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(allRequestsArray.get(i).getTime()));
        cardViewHolder.pointDepartureText.setText(allRequestsArray.get(i).getFrom().getAdress());
        cardViewHolder.pointDestinationText.setText(allRequestsArray.get(i).getTo().getAdress());
    }

    @Override
    public int getItemCount() {
        return allRequestsArray.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
