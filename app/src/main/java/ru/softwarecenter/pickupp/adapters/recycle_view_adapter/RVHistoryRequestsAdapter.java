package ru.softwarecenter.pickupp.adapters.recycle_view_adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.interfaces.OnLoadMoreListener;
import ru.softwarecenter.pickupp.models.Ride;

public class RVHistoryRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView dateText, timeText, pointDepartureText, pointDestinationText;

        CardViewHolder(View itemView) {
            super(itemView);

            cv = itemView.findViewById(R.id.idCardRequest);
            dateText = itemView.findViewById(R.id.idCardDateText);
            timeText = itemView.findViewById(R.id.idCardTimeText);
            pointDepartureText = itemView.findViewById(R.id.idPointOfDepartureText);
            pointDestinationText = itemView.findViewById(R.id.idPointOfDestinationText);
        }
    }

    public static class LoadingRidesViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;
        public LoadingRidesViewHolder(View itemView) {
            super(itemView);

            progressBar = itemView.findViewById(R.id.progressBar1);
        }
    }



    private List<Ride> allRequestsArray;
    public RVHistoryRequestsAdapter(List<Ride> allRequestsArray, RecyclerView mRecyclerView){
        this.allRequestsArray = allRequestsArray;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override public int getItemViewType(int position) {
        return allRequestsArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        /*View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_history_requests, viewGroup, false);
        RVHistoryRequestsAdapter.CardViewHolder cvh = new RVHistoryRequestsAdapter.CardViewHolder(v);
        return cvh;*/

        if (i == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_history_requests, viewGroup, false);
            return new CardViewHolder(view);
        } else if (i == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loading_item_layout, viewGroup, false);
            return new LoadingRidesViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof CardViewHolder) {
            Ride ride = allRequestsArray.get(i);
            CardViewHolder cardViewHolder = (CardViewHolder) holder;
            cardViewHolder.dateText.setText(new java.text.SimpleDateFormat("d MMM").format(ride.getTime()));
            cardViewHolder.timeText.setText(new java.text.SimpleDateFormat("HH:mm", Locale.ENGLISH).format(ride.getTime()));
            cardViewHolder.pointDepartureText.setText(ride.getFrom().getAdress());
            cardViewHolder.pointDestinationText.setText(ride.getTo().getAdress());
        } else if (holder instanceof LoadingRidesViewHolder) {
            LoadingRidesViewHolder loadingViewHolder = (LoadingRidesViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return allRequestsArray == null ? 0 : allRequestsArray.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setLoaded() {
        isLoading = false;
    }
}
