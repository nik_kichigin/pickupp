package ru.softwarecenter.pickupp.adapters.expandable_list_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.softwarecenter.pickupp.R;
import ru.softwarecenter.pickupp.models.User;

public class SearchRideUserAdapter extends BaseExpandableListAdapter {

    private Context context;
    private LayoutInflater infalInflater;
    private List<User> listDataHeader = new ArrayList<>();
    private HashMap<User, List<User>> listDataChild = new HashMap<>();

    public SearchRideUserAdapter(Context context) {
        this.context = context;
        infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addUsers(List<User> data){
        if(data.size() > 2){
            User user = new User();
            listDataHeader.addAll(data.subList(0, 2));
            listDataHeader.add(user);
            listDataChild.put(data.get(0), null);
            listDataChild.put(data.get(1), null);
            listDataChild.put(user, data.subList(2, data.size()));
            notifyDataSetChanged();
        }else{
            listDataHeader.addAll(data);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 0;
        try{
            count = this.listDataChild.get(this.listDataHeader.get(groupPosition))
                    .size();
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup viewGroup) {
        if(getChildrenCount(groupPosition) > 0){
            convertView = infalInflater.inflate(R.layout.expandable_user_list_item, viewGroup, false);
            if(isExpanded){
                convertView.findViewById(R.id.expandableIcon).setRotation(180);
            }else{
                convertView.findViewById(R.id.expandableIcon).setRotation(0);
            }
            return convertView;
        }else{
            if(groupPosition % 2 == 0){
                convertView = infalInflater.inflate(R.layout.user_man_small_item, viewGroup, false);

                return convertView;
            }else{
                convertView = infalInflater.inflate(R.layout.user_woman_small_item, viewGroup, false);
                return convertView;
            }
        }
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isExpanded, View convertView, ViewGroup viewGroup) {
        convertView = infalInflater.inflate(R.layout.user_man_small_item, viewGroup, false);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
