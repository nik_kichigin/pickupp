package ru.softwarecenter.pickupp.models;

public class AdressSearch {
    private String street_home;
    private String city_country;

    public String getStreet_home() {
        return street_home;
    }

    public String getCity_country() {
        return city_country;
    }

    public void setStreet_home(String street_home) {
        this.street_home = street_home;
    }

    public void setCity_country(String city_country) {
        this.city_country = city_country;
    }
}
