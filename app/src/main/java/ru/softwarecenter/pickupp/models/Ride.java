package ru.softwarecenter.pickupp.models;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.List;

import ru.softwarecenter.pickupp.utils.Constants;

public class Ride implements Parcelable {
    private String id;
    private String name;
    private int status;
    private List<User> users;
    private Street from;
    private Street to;
    private long time;
    private int earlyTime;
    private int lateTime;
    private boolean driverOn;
    private int radiusBefore = 0;
    private int rediusAfter = 0;
    private int cost = 0;
    private int people = 0;
    private boolean hasBag = false;
    private int searchPickUpp = -1;

    public Ride() {
        from = null;
        to = null;
        time = Calendar.getInstance().getTimeInMillis() + 15 * Constants.ONE_MINUTE_IN_MS;
        driverOn = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Street getFrom() {
        return from;
    }

    public void setFrom(Street from) {
        this.from = from;
    }

    public Street getTo() {
        return to;
    }

    public void setTo(Street to) {
        this.to = to;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isDriverOn() {
        return driverOn;
    }

    public void setDriverOn(boolean driverOn) {
        this.driverOn = driverOn;
    }

    public int getRadiusBefore() {
        return radiusBefore;
    }

    public void setRadiusBefore(int radiusBefore) {
        this.radiusBefore = radiusBefore;
    }

    public int getRediusAfter() {
        return rediusAfter;
    }

    public void setRediusAfter(int rediusAfter) {
        this.rediusAfter = rediusAfter;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getPeople() {
        return people;
    }

    public void setPeople(int people) {
        this.people = people;
    }

    public boolean isHasBag() {
        return hasBag;
    }

    public void setHasBag(boolean hasBag) {
        this.hasBag = hasBag;
    }

    public int getSearchPickUpp() {
        return searchPickUpp;
    }

    public void setSearchPickUpp(int searchPickUpp) {
        this.searchPickUpp = searchPickUpp;
    }

    public int getEarlyTime() {
        return earlyTime;
    }

    public int getLateTime() {
        return lateTime;
    }

    public void setEarlyTime(int earlyTime) {
        this.earlyTime = earlyTime;
    }

    public void setLateTime(int lateTime) {
        this.lateTime = lateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.status);
        dest.writeTypedList(this.users);
        dest.writeParcelable(this.from, flags);
        dest.writeParcelable(this.to, flags);
        dest.writeLong(this.time);
        dest.writeByte(this.driverOn ? (byte) 1 : (byte) 0);
        dest.writeInt(this.radiusBefore);
        dest.writeInt(this.rediusAfter);
        dest.writeInt(this.lateTime);
        dest.writeInt(this.earlyTime);
        dest.writeInt(this.cost);
        dest.writeInt(this.people);
        dest.writeByte(this.hasBag ? (byte) 1 : (byte) 0);
        dest.writeInt(this.searchPickUpp);
    }

    protected Ride(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.status = in.readInt();
        this.users = in.createTypedArrayList(User.CREATOR);
        this.from = in.readParcelable(Street.class.getClassLoader());
        this.to = in.readParcelable(Street.class.getClassLoader());
        this.time = in.readLong();
        this.driverOn = in.readByte() != 0;
        this.radiusBefore = in.readInt();
        this.rediusAfter = in.readInt();
        this.cost = in.readInt();
        this.people = in.readInt();
        this.hasBag = in.readByte() != 0;
        this.searchPickUpp = in.readInt();
    }

    public static final Creator<Ride> CREATOR = new Creator<Ride>() {
        @Override
        public Ride createFromParcel(Parcel source) {
            return new Ride(source);
        }

        @Override
        public Ride[] newArray(int size) {
            return new Ride[size];
        }
    };
}
