package ru.softwarecenter.pickupp.models.expandable_list_models;

import java.util.ArrayList;
import java.util.List;

public class AboutTravelerGroupItem {
    public String title;
    public List<AboutTravelerChildItem> items = new ArrayList<AboutTravelerChildItem>();
}
