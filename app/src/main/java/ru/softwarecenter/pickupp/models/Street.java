package ru.softwarecenter.pickupp.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class Street implements Parcelable {
    private String adress;
    private LatLng point;

    public Street buildStreet(String adress, LatLng point){
        this.adress = adress;
        this.point = point;
        return this;
    }

    public String getAdress() {
        return adress;
    }

    public LatLng getPoint() {
        return point;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setPoint(LatLng point) {
        this.point = point;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.adress);
        dest.writeParcelable(this.point, flags);
    }

    public Street() {
    }

    protected Street(Parcel in) {
        this.adress = in.readString();
        this.point = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Parcelable.Creator<Street> CREATOR = new Parcelable.Creator<Street>() {
        @Override
        public Street createFromParcel(Parcel source) {
            return new Street(source);
        }

        @Override
        public Street[] newArray(int size) {
            return new Street[size];
        }
    };
}
