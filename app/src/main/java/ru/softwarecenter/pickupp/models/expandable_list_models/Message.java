package ru.softwarecenter.pickupp.models.expandable_list_models;

public class Message {
    private String message;
    private String time;
    private int id;
    private String owner_user_message;
    private int sender_user_id;
    private int recipient_user_id;

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public String getTime() {
        return time;
    }

    public int getId() {
        return id;
    }

    public String getOwner_user_message() {
        return owner_user_message;
    }

    public int getSender_user_id() {
        return sender_user_id;
    }

    public int getRecipient_user_id() {
        return recipient_user_id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOwner_user_message(String owner_user_message) {
        this.owner_user_message = owner_user_message;
    }

    public void setSender_user_id(int sender_user_id) {
        this.sender_user_id = sender_user_id;
    }

    public void setRecipient_user_id(int recipient_user_id) {
        this.recipient_user_id = recipient_user_id;
    }
}
