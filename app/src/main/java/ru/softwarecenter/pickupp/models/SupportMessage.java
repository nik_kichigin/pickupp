package ru.softwarecenter.pickupp.models;

public class SupportMessage {

    private String category;
    private String communication_path;
    private String email;
    private String message;

    public String getCategory() {
        return category;
    }

    public String getCommunication_path() {
        return communication_path;
    }

    public String getEmail() {
        return email;
    }

    public String getMessage() {
        return message;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCommunication_path(String communication_path) {
        this.communication_path = communication_path;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
