package ru.softwarecenter.pickupp.models;


import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String name = null;
    private int age = -1;
    private int sex = -1;
    private String avatar = null;
    private String status = null;
    private String youselfText = null;
    private String additionalRideInfo = null;
    private int starsCount = 0;
    private int raiting = 0;
    private int id = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getYouselfText() {
        return youselfText;
    }

    public void setYouselfText(String youselfText) {
        this.youselfText = youselfText;
    }

    public String getAdditionalRideInfo() {
        return additionalRideInfo;
    }

    public void setAdditionalRideInfo(String additionalRideInfo) {
        this.additionalRideInfo = additionalRideInfo;
    }

    public int getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(int starsCount) {
        this.starsCount = starsCount;
    }

    public int getRaiting() {
        return raiting;
    }

    public void setRaiting(int raiting) {
        this.raiting = raiting;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.age);
        dest.writeInt(this.sex);
        dest.writeString(this.avatar);
        dest.writeString(this.status);
        dest.writeString(this.youselfText);
        dest.writeString(this.additionalRideInfo);
        dest.writeInt(this.starsCount);
        dest.writeInt(this.raiting);
        dest.writeInt(this.id);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.age = in.readInt();
        this.sex = in.readInt();
        this.avatar = in.readString();
        this.status = in.readString();
        this.youselfText = in.readString();
        this.additionalRideInfo = in.readString();
        this.starsCount = in.readInt();
        this.raiting = in.readInt();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
